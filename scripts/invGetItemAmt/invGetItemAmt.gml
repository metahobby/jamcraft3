/// @description Provides the amount of an item in the inventory.
/// @function invGetItemAmt(inv, itemID);
/// @param inv
/// @param itemID

var _inv = argument0;
var _id = argument1;

var slot = 0; //A temporary variable to loop through the slots
var invWid = ds_grid_width(_inv);
//TODO expand this to work with items that have stack limits. Tally up all inv slots that have that ID.
while slot < invWid {
    //Searching for a matching inventory item
    if _inv[# slot, 0] == _id {
        return _inv[# slot, invStat.amt];
    }
    slot++; //Increment slot to test next position
}
return 0;
