///@func drawLayerText(str, x, y, halign, valign, opt-midcolor)

var str = argument0; ///@param string
var _x = argument1; ///@param x
var _y = argument2; ///@param y
draw_set_font(argument3); /// @param font
draw_set_halign(argument4); ///@param halign
draw_set_valign(argument5); ///@param valign
var botColor = argument6; ///@param botColor
var midColor = argument7; ///@param midColor
var topColor = argument8; ///@param topColor
	
draw_set_color(botColor);
draw_text(_x - 1, _y + 1, str);
draw_set_color(midColor);
draw_text(_x, _y, str);
draw_set_color(topColor);
draw_text(_x + 1, _y - 1, str);	
