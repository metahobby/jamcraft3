/// @description invAddMaterial(itemID, name, description, stackMax);
/// @function invAddWeapon
/// @param itemID
/// @param name
/// @param description
/// @param stackMax


var _id = argument0;
oGame.itemDex[# _id, itemStat.name] = argument1;
oGame.itemDex[# _id, itemStat.description] = argument2;
oGame.itemDex[# _id, itemStat.stackMax] = argument3;
oGame.itemDex[# _id, itemStat.type] = itemType.material;
