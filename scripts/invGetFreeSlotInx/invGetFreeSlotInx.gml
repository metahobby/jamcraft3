/// @description Provides an index of a free slot or -1.
/// @function invGetItemAmt(inv);
/// @param inv


var _inv = argument0;
var _slot = 0; //A temporary variable to loop through the slots
var _max = 0;

while _slot < _max {
    if _inv[# _slot, 0] == items.none {
        return _slot;
    }
    _slot++; //increase slot
}
return -1; //return free amount
