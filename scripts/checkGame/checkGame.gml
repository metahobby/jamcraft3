/// @description mouse_over()
///Returns true if cursor is within object's bounding box
var mX = device_mouse_x(0);
var mY = device_mouse_y(0)

return (mX >= bbox_left &&
        mX <= bbox_right &&
        mY >= bbox_top &&
        mY <= bbox_bottom &&
		mouse_check_button(mb_any))
