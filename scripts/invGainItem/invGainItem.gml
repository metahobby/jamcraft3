/// @description Adds an item and a quantity into the inventory in a valid slot.
/// @function invAddItem(inv, itemID, amount, override);
/// @param inv
/// @param itemID
/// @param amount
/// @param quality
/// @param override

var _inv = argument0;
var _id = argument1;
var amount = argument2;
var override = argument3;

var slot = 0; //A temporary variable to loop through the slots
var invWid = ds_grid_width(_inv);

while slot < invWid {
    //Searching for a matching inventory slot
    if _inv[# slot, invStat.item] == _id && oGame.itemDex[# _inv[# slot, invStat.item], itemStat.stackMax] != 1 {
		if override {
			_inv[# slot, invStat.amt] = amount;
		} else {
			_inv[# slot, invStat.amt] += amount;
		}
		//_inv[# slot, invStat.quality] = quality;
		var stackMax = invGetItemStackMax(_inv[# slot, invStat.item])
		if stackMax > 0 {
			_inv[# slot, invStat.amt] = clamp(_inv[# slot, invStat.amt], 0, stackMax);
		}
        return true; //Did set the slot
    }
    slot++; //Increment slot to test next position
}

slot = 0;
while (slot < invWid) {
    //Searching for an empty inventory slot
    if _inv[# slot, invStat.item] == items.none {
        _inv[# slot, invStat.item] = _id;
        _inv[# slot, invStat.amt] += amount;
		var stackMax = invGetItemStackMax(_inv[# slot, invStat.item])
		if stackMax > 0 {
			_inv[# slot, invStat.amt] = clamp(_inv[# slot, invStat.amt], 0, stackMax);
		}
		//_inv[# slot, invStat.quality] = quality;
        return true; //Did set the slot
    }
    slot++; //Increment slot to test next position
}
return false; //Did not set slot