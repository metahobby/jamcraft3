enum	menuEleType {
	button,
	multiselect,
	slider,
	shifter,
	toggle,
	input,
}

enum audioGrp {
	mas,
	sfx,
	bgm
}

enum drawE {
	gui,
	game,
}

enum objDepth {
	background = 200,
	plat = 100,
	board = 0,
	defInst = -100,
	lines = -100,
	finishers = -200,
	nodes = -300,
	gameUI = -1000,
	gui = -10000
}

enum fontType {
	original,
	readable
}

enum uiAnchor {
	topLeft,
	topCenter,
	topRight,
	middleLeft,
	middleCenter,
	middleRight,
	bottomLeft,
	bottomCenter,
	bottomRight,
}
