enum items {
	none,
	coalRock,
	coal,
	powerBox,
	pipe,
	conduit,
	LED,
	hangLight,
	bigLight,
	smallLight,
	pcb,
	powerBoxLeft,
	powerBoxRight,
	quartz,
	quartzRock,
	copper,
	gold,
	iron,
	titanium,
	silicon,
	siliconOre,
	SiliconRock,
	solar,
	waffer,
	wire,
	total
}

enum itemStat {
	name,
	description,
	type,
	damage,
	effectScript,
	quality,
	stackMax,
	value,
	total
}


enum itemType {
	none,
	tool,
	material,
	consumable,
	total
}

enum invStat {
	item,
	amt,
	quality,
	total,
}

enum slotType {
	normal,
}
