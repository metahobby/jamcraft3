/// @description Provides the max amount of an item that can be in the inventory.
/// @function invModAmt(slot, amount, override);
/// @param itemID

var _id = argument0;
return oGame.itemDex[# _id, itemStat.stackMax];
