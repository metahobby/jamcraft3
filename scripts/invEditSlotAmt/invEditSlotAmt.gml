/// @description Modifies a slot in the inventory. Can add and remove items, and set the item.
/// @function invEditSlotAmt(inv, slot, amount, quality, override);
/// @param inv
/// @param slot
/// @param amount
/// @param quality
/// @param override

//Assign local variables
var _inv = argument0;
var slot = argument1;
var amount = argument2;
var quality = argument3;
var override = argument4;
//If it is not overriding current values
if (override == false) {
	_inv[# slot, invStat.amt] += amount; //Increase amount by input amount
	_inv[# slot, invStat.quality] += quality;
//If it is overriding current values
} else {
	_inv[# slot, 1] = amount; //Set amount to input amount
	_inv[# slot, invStat.quality] = quality;
}
var stackMax = invGetItemStackMax(_inv[# slot, invStat.item])
if stackMax > 0 {
	_inv[# slot, invStat.amt] = clamp(_inv[# slot, invStat.amt], 0, stackMax);
}

//Clear slot if the amount is less than or equal to 0
if (_inv[# slot, invStat.amt] <= 0) {
	_inv[# slot, invStat.item] = items.none;
	_inv[# slot, invStat.amt] = 0;
	_inv[# slot, invStat.quality] = 0;
}