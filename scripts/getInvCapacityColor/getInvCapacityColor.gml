
var _cur = argument0;
var _max = argument1;

if _cur < _max / 2 {
	return c_white;
} else if _cur >= _max / 2 && _cur < _max - _max / 10 {
	return c_yellow
} else {
	return c_red;
}