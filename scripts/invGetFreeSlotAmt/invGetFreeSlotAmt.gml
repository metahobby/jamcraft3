/// @description Provides the amount of empty inventory slots.
/// @function invGetItemAmt(inv);
/// @param inv


var _inv = argument0;

var free = 0; //Free slot counter
var slot = 0; //A temporary variable to loop through the slots
var invWid = ds_grid_width(_inv);

while slot < invWid {
    if _inv[# slot, 0] != items.none {
        free++; //was free
    }
    slot++; //increase slot
}
return free; //return free amount
