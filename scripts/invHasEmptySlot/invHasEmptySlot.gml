/// @description returns true if there is an empty slot in the inventory.
/// @function invHasEmptySlot
/// @param inv

//Assign local variables
var _inv = argument0;
//If it is not overriding current values
var slot = 0; //A temporary variable to loop through the slots
var _max;

if _inv == oGame.inv {
	_max = oGame.invMax;
} else if _inv == oGame.marketInv {
	_max = oGame.marketInvMax;
} else {
	_max = ds_grid_width(_inv);
}

while slot < _max {
    //Searching for a empty
    if _inv[# slot, invStat.item] == items.none {
		return true;
    }
    slot++; //Increment slot to test next position
}
return false;
