
var item = argument0;
var curDur = argument1;

var maxDur = oGame.itemDex[# item, itemStat.quality];
if maxDur <= 0 || curDur <= 0 {
	return oGame.qcArr[0];
}
var curPerc = curDur / maxDur;
var cLen = array_length_1d(oGame.qcArr) - 2;
var cIndex = round(cLen * curPerc);
var color = oGame.qcArr[cIndex];
return color
