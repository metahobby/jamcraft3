{
    "id": "63d8aefc-b069-40d6-a55e-8da37a642ea9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSunWell",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 209,
    "bbox_left": 0,
    "bbox_right": 151,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7a593cf9-031a-495e-99bc-c94966bb4e14",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63d8aefc-b069-40d6-a55e-8da37a642ea9",
            "compositeImage": {
                "id": "e5127268-1465-4fdd-b2d6-4148f70a2582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a593cf9-031a-495e-99bc-c94966bb4e14",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "184add81-b86f-4f09-b1f6-aca8a9b4457a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a593cf9-031a-495e-99bc-c94966bb4e14",
                    "LayerId": "f21e6e8f-867f-4241-8095-de939c605f11"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 210,
    "layers": [
        {
            "id": "f21e6e8f-867f-4241-8095-de939c605f11",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63d8aefc-b069-40d6-a55e-8da37a642ea9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 152,
    "xorig": 76,
    "yorig": 209
}