{
    "id": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprDrill",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 34,
    "bbox_left": 0,
    "bbox_right": 45,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1d7cac13-3637-46f8-ae49-bc4319766fd3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "469956a5-a4ea-4195-8fed-34637ff8dbc3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d7cac13-3637-46f8-ae49-bc4319766fd3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b6ec2f9-9b25-4ea2-afe2-c20bbfd37714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d7cac13-3637-46f8-ae49-bc4319766fd3",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "791a544f-a864-461e-b31d-6b130d40cc90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "98beb8c1-3057-409b-9395-0984e9f1e372",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "791a544f-a864-461e-b31d-6b130d40cc90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5d28222-5528-4122-87d8-45fbd381e20d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "791a544f-a864-461e-b31d-6b130d40cc90",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "7978e79e-7874-411c-bb67-a2f9e4bf3c25",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "f67ad510-a019-412e-9e4a-aed7bb2272a1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7978e79e-7874-411c-bb67-a2f9e4bf3c25",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f0bbcb96-869e-4e7b-898d-ae581ea2bf2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7978e79e-7874-411c-bb67-a2f9e4bf3c25",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "36bc179a-3040-4190-8cb9-09524efe90f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "5b7b3542-288c-4518-8270-3ecfb83a6e4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "36bc179a-3040-4190-8cb9-09524efe90f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79d61264-60bb-4531-835c-eacf7ef2ef54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "36bc179a-3040-4190-8cb9-09524efe90f7",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "0ef3970c-bd1c-4d22-9e91-2fa4989a433f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "e97294b4-4529-42ae-93f2-ff694d2ecbda",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0ef3970c-bd1c-4d22-9e91-2fa4989a433f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "44a7b6b3-af5b-493c-858e-df1c5b5713ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0ef3970c-bd1c-4d22-9e91-2fa4989a433f",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "5b35f562-6457-4397-8c9d-96b6df605098",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "e4228e8d-13d8-4a7c-99cb-293a9984fd65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b35f562-6457-4397-8c9d-96b6df605098",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b444c246-bd19-4276-adbb-173005cd9a50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b35f562-6457-4397-8c9d-96b6df605098",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "f6419db8-c432-407c-9d6f-5d746ed797dc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "5b5c6a18-9196-44e9-9e8f-990df3b887a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f6419db8-c432-407c-9d6f-5d746ed797dc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7cf9af8e-6ab0-455c-afcf-46391fc90cd6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f6419db8-c432-407c-9d6f-5d746ed797dc",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "cc9bde15-2018-4cf2-bcc6-482ce750ab51",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "c3d757b7-1654-47fa-b737-1f5508c3ec85",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cc9bde15-2018-4cf2-bcc6-482ce750ab51",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7525aea8-af43-4aa0-8c90-79044157c623",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cc9bde15-2018-4cf2-bcc6-482ce750ab51",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "0909bc5f-66a6-49a0-9d0d-03bf036bebf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "d246e43c-dadf-4d0c-a1c1-8bd5e2a2e582",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0909bc5f-66a6-49a0-9d0d-03bf036bebf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "134ca325-e8a4-4a9f-8db6-f5ee61f9f609",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0909bc5f-66a6-49a0-9d0d-03bf036bebf1",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "9e57c63d-f1d8-4b3b-8820-fd62aeaf4729",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "e74add77-e1da-44a6-b44a-6dc169c5d971",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e57c63d-f1d8-4b3b-8820-fd62aeaf4729",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29550f33-2cc8-439b-8c89-dc2d840b415e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e57c63d-f1d8-4b3b-8820-fd62aeaf4729",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "525ddc8a-1d77-454a-ab85-d3b2e758d1c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "c212a0a6-e36e-4351-9cc3-720be437a1c4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "525ddc8a-1d77-454a-ab85-d3b2e758d1c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "787c7111-8829-4f5f-854f-85a3fb9f18e1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "525ddc8a-1d77-454a-ab85-d3b2e758d1c9",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        },
        {
            "id": "3b177268-ccdd-49c7-b6f1-023c99d38db2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "compositeImage": {
                "id": "8cf784f6-830a-49ad-b605-213918ff4661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3b177268-ccdd-49c7-b6f1-023c99d38db2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f340d99-05fa-4049-9aa7-5ae84a969731",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3b177268-ccdd-49c7-b6f1-023c99d38db2",
                    "LayerId": "7e0113db-66fb-4b27-81a7-33fa826d5c81"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 35,
    "layers": [
        {
            "id": "7e0113db-66fb-4b27-81a7-33fa826d5c81",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5d90244f-d71b-4640-ad4f-7a8b9fb8f323",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 8,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 46,
    "xorig": 45,
    "yorig": 34
}