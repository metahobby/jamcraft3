{
    "id": "b6c7ad08-e227-42f4-86d0-6061f4747a23",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSiliconRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dbe6d00e-18d2-4d0c-878b-25a5c74f480d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b6c7ad08-e227-42f4-86d0-6061f4747a23",
            "compositeImage": {
                "id": "63e25b9b-6bb0-4e08-abe6-ab7bdf192565",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dbe6d00e-18d2-4d0c-878b-25a5c74f480d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "affd8781-57b1-40db-867e-f3b4c35a7965",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dbe6d00e-18d2-4d0c-878b-25a5c74f480d",
                    "LayerId": "231af229-5efc-441c-87b2-13ab5dd42ddb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "231af229-5efc-441c-87b2-13ab5dd42ddb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b6c7ad08-e227-42f4-86d0-6061f4747a23",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}