{
    "id": "e4592732-252f-449e-8f22-c686e0ecd4ec",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "27d7418f-0de6-48fe-b58c-0dbe4ea1b368",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4592732-252f-449e-8f22-c686e0ecd4ec",
            "compositeImage": {
                "id": "d21f60d7-1089-4449-93e6-81321512335f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27d7418f-0de6-48fe-b58c-0dbe4ea1b368",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fe03d1ac-3761-40f1-adea-dba3118c4360",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27d7418f-0de6-48fe-b58c-0dbe4ea1b368",
                    "LayerId": "78f25332-0cd9-4506-a1f5-acabab4a88cc"
                }
            ]
        },
        {
            "id": "b58f4201-e372-40a1-a69b-755dbd4aa200",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4592732-252f-449e-8f22-c686e0ecd4ec",
            "compositeImage": {
                "id": "7d2a665a-512d-4bca-b545-02c210095e40",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b58f4201-e372-40a1-a69b-755dbd4aa200",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "96780976-4ddd-4420-90c5-0fe1372054cd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b58f4201-e372-40a1-a69b-755dbd4aa200",
                    "LayerId": "78f25332-0cd9-4506-a1f5-acabab4a88cc"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "78f25332-0cd9-4506-a1f5-acabab4a88cc",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4592732-252f-449e-8f22-c686e0ecd4ec",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}