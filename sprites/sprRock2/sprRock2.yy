{
    "id": "63260c39-a949-44cd-b84b-6292a902724e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRock2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d6b4da9d-af8d-417c-b229-1be5a2f235c2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "63260c39-a949-44cd-b84b-6292a902724e",
            "compositeImage": {
                "id": "14efe2c3-e44e-4998-8fd1-6c894bb526ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6b4da9d-af8d-417c-b229-1be5a2f235c2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cc6bc857-3c10-4580-b01a-75c15a4fd95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6b4da9d-af8d-417c-b229-1be5a2f235c2",
                    "LayerId": "0294e662-f0ba-47f0-a245-60e3e5f4be1c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "0294e662-f0ba-47f0-a245-60e3e5f4be1c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "63260c39-a949-44cd-b84b-6292a902724e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}