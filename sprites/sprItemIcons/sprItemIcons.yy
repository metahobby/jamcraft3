{
    "id": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprItemIcons",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "338ffe89-147d-429f-bbc9-300dd33976f5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "6dd3c34e-5b22-4d9d-b7cf-aa2442d23187",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "338ffe89-147d-429f-bbc9-300dd33976f5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "55bcf0fe-a466-49cf-9ab6-4b2e2f27d5a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "338ffe89-147d-429f-bbc9-300dd33976f5",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "81dd0fc3-38e8-40b2-b8ee-45a60c64078a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "f3b769ca-2a78-4cf3-9b13-bd7cc7517fa7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "81dd0fc3-38e8-40b2-b8ee-45a60c64078a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "512ba3fb-471b-4e0f-96c2-cec7dc521e4d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "81dd0fc3-38e8-40b2-b8ee-45a60c64078a",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "95f8ccb2-bd3d-4929-8f86-53f3f12fef77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "cbd4e766-7d8e-40ee-97c6-9e3d20926d2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95f8ccb2-bd3d-4929-8f86-53f3f12fef77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5a2088f1-349e-4663-ac7b-77c874ffa2cc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95f8ccb2-bd3d-4929-8f86-53f3f12fef77",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "e653fa6e-4bde-4a54-b05d-f58061c72b70",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "a238bf4d-0bf9-4633-8043-da2d6f66d465",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e653fa6e-4bde-4a54-b05d-f58061c72b70",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5fb6ccd9-669c-41e5-9ebf-e1f5d075987b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e653fa6e-4bde-4a54-b05d-f58061c72b70",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "8bb697d2-b6fc-40e1-a2af-53a456c91afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "a3ad02fc-e8d0-4c32-983c-858b9be6b8a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8bb697d2-b6fc-40e1-a2af-53a456c91afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ee8d43d9-e627-45fd-8df9-6c4044ea4805",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8bb697d2-b6fc-40e1-a2af-53a456c91afc",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "b462b44a-eed9-45c9-9d2e-73a6f5124144",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "58b83ba6-23f1-4b0d-b80b-6f6802f8a0fe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b462b44a-eed9-45c9-9d2e-73a6f5124144",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b67e6e22-3486-479d-b4e1-f91cd7556ae3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b462b44a-eed9-45c9-9d2e-73a6f5124144",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "eb3cd9fb-35d6-42c5-ad38-b45a0f146f31",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "12aed2ea-38ba-44c3-94b8-1a54434c1d27",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eb3cd9fb-35d6-42c5-ad38-b45a0f146f31",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8af825c-8de9-4a0c-bb87-29a64697dafb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eb3cd9fb-35d6-42c5-ad38-b45a0f146f31",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "dcd1e9b1-992b-4588-9d97-aa0dc38ecb56",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "aefd2c13-9a81-464c-aed5-5be42868c1b1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcd1e9b1-992b-4588-9d97-aa0dc38ecb56",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d99c4e0-66a3-4697-9957-5ae946a85926",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcd1e9b1-992b-4588-9d97-aa0dc38ecb56",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "7a60309f-78fa-480f-85fc-dbd7bb898cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "c8171fc4-d3a4-4656-9d57-1504b93fe8ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7a60309f-78fa-480f-85fc-dbd7bb898cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c8d3a1c-8a14-4517-a38a-b32f14a608aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7a60309f-78fa-480f-85fc-dbd7bb898cf0",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "90ab370e-2bc2-4f43-8b74-49443d89e85b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "0a7d1e87-7967-4dc3-9ba1-337c0b4be797",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90ab370e-2bc2-4f43-8b74-49443d89e85b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a6cd76f-3195-465a-a1b3-7b14e11ce087",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90ab370e-2bc2-4f43-8b74-49443d89e85b",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "57bcff63-475a-4ae7-85b1-5b9c1f868cca",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "5b317289-5abb-40c8-ac3c-4f3cba7fb123",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57bcff63-475a-4ae7-85b1-5b9c1f868cca",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3b07e59-a002-404f-b1eb-d07c4216104b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57bcff63-475a-4ae7-85b1-5b9c1f868cca",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "7326e036-7f33-4e64-bc1b-4599ce7b9797",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "71f643ee-2a5f-4cc2-a320-a6f94e52ca73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7326e036-7f33-4e64-bc1b-4599ce7b9797",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1465190e-54f9-43a2-b78c-72c065b41898",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7326e036-7f33-4e64-bc1b-4599ce7b9797",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "7051f6a7-2a56-4d5a-a7f0-f0c5a5a73731",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "37d2e651-4885-42dd-94d7-85777665a9a7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7051f6a7-2a56-4d5a-a7f0-f0c5a5a73731",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be82fa2d-a5eb-4f9d-879c-bf3a2e094ee0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7051f6a7-2a56-4d5a-a7f0-f0c5a5a73731",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "c83b34c5-ff01-4494-a5c2-9421fae9a53e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "cfd6e9ac-bcac-40d0-b2e4-77a6045d26fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c83b34c5-ff01-4494-a5c2-9421fae9a53e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb7cce8a-e2ef-4321-8f60-c2ec05a9f304",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c83b34c5-ff01-4494-a5c2-9421fae9a53e",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "2a04c27e-d7a3-489d-a4b9-ceaa7a81cbd5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "749fceec-6b8c-47f6-8050-381143ac4364",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a04c27e-d7a3-489d-a4b9-ceaa7a81cbd5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b0ec22d-79b7-41db-843f-f95744631e80",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a04c27e-d7a3-489d-a4b9-ceaa7a81cbd5",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "6ba4e1d3-343e-4ae1-9a84-c6eaf9be1314",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "03c8c0ee-9fe8-48af-9141-f3e85a686d55",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6ba4e1d3-343e-4ae1-9a84-c6eaf9be1314",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bbc8a25-9b80-4d65-9d7b-0dbe161dbf92",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6ba4e1d3-343e-4ae1-9a84-c6eaf9be1314",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "27b7a5ac-e6df-4d4b-a740-e1d14e34e5bd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "0a21f2b1-5401-4f17-80e6-b9ee8a79051f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "27b7a5ac-e6df-4d4b-a740-e1d14e34e5bd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d48fd016-e233-43a5-9e43-1039c2b365c6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "27b7a5ac-e6df-4d4b-a740-e1d14e34e5bd",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "bb60e929-48c9-4a50-8500-c58dd29a7a53",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "820a77a1-685b-4021-ac54-5f33d27e4b04",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bb60e929-48c9-4a50-8500-c58dd29a7a53",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1fbce1fd-64c3-4294-a208-ef386131bade",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bb60e929-48c9-4a50-8500-c58dd29a7a53",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "801d8f4f-e440-46b3-b589-5f6e39f1ae28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "24ebcdcf-35a3-4e53-bbec-440ea5a8cb60",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "801d8f4f-e440-46b3-b589-5f6e39f1ae28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "01824a86-ad0b-4465-9fe6-c70de58a8c29",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "801d8f4f-e440-46b3-b589-5f6e39f1ae28",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "73f8b963-591b-455d-962a-0762ca2ef133",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "63a6b1f9-0278-49cd-a928-655f56e5ce97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "73f8b963-591b-455d-962a-0762ca2ef133",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "34400d40-1029-4286-a626-880c6e434381",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "73f8b963-591b-455d-962a-0762ca2ef133",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "ca4ca69b-9c02-4591-a7f0-3bbf81952926",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "00c30084-be67-494a-bb04-1a93140e7d7a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca4ca69b-9c02-4591-a7f0-3bbf81952926",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4d8d3c5f-8e06-47ea-9a61-53b949054996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca4ca69b-9c02-4591-a7f0-3bbf81952926",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "48255394-c7d8-494f-9b85-da18bc8a57bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "81b6cd06-2d0d-4976-976f-5e7cb11b2741",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "48255394-c7d8-494f-9b85-da18bc8a57bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e5e9015d-d588-4259-a7ce-439b7c14a6d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "48255394-c7d8-494f-9b85-da18bc8a57bf",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "95bfc9ed-2c11-4299-9063-7acbc0fa35eb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "f4cd794b-6e5b-48cc-a520-a8887e139652",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95bfc9ed-2c11-4299-9063-7acbc0fa35eb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18bdf8a4-421b-4dc6-b0b3-a724da52104c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95bfc9ed-2c11-4299-9063-7acbc0fa35eb",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "8fa48bb6-96ea-49c8-9fe1-5b6fb59dea75",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "acb02c4d-9d30-4836-9e5b-fef5d4214dc7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8fa48bb6-96ea-49c8-9fe1-5b6fb59dea75",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "466f61ac-254c-494e-b168-52eee40091ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8fa48bb6-96ea-49c8-9fe1-5b6fb59dea75",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        },
        {
            "id": "4a33575d-91fd-49f2-ad20-2224f59bc6d1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "compositeImage": {
                "id": "68b21c8b-60e4-49c2-a0f7-29d372d1f127",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a33575d-91fd-49f2-ad20-2224f59bc6d1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a6fd5998-6c68-4c10-9d88-ec9b4238f11b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a33575d-91fd-49f2-ad20-2224f59bc6d1",
                    "LayerId": "196aac6b-0e78-4916-a7dc-4d22327e0d67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "196aac6b-0e78-4916-a7dc-4d22327e0d67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d9139df-4a3b-44cd-a202-3e4f5c5dd3bb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}