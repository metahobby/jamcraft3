{
    "id": "614bb2e0-c712-4ffc-ae50-80db1c2f82b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMiss",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c1fdb127-547b-4f71-ab37-e5fb1e954ad4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "614bb2e0-c712-4ffc-ae50-80db1c2f82b9",
            "compositeImage": {
                "id": "a8b96c0d-f290-4b1d-a4e0-8c13ad9aa7e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c1fdb127-547b-4f71-ab37-e5fb1e954ad4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7fad678-7e93-47cf-8135-d0beb760a09f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c1fdb127-547b-4f71-ab37-e5fb1e954ad4",
                    "LayerId": "1cd0d988-fec7-49cf-89e9-4a4f6ec2082b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "1cd0d988-fec7-49cf-89e9-4a4f6ec2082b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "614bb2e0-c712-4ffc-ae50-80db1c2f82b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}