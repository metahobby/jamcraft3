{
    "id": "37d02ee1-99cc-4ce5-8aec-f89940cc5cd3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 14,
    "bbox_right": 83,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6f0dc4fb-c38f-4579-83a0-49c3585a0c77",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d02ee1-99cc-4ce5-8aec-f89940cc5cd3",
            "compositeImage": {
                "id": "0572545e-8abe-4012-89c7-a622cd68588f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6f0dc4fb-c38f-4579-83a0-49c3585a0c77",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ce63e608-8d89-49cf-9e70-12aeaf15eed8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6f0dc4fb-c38f-4579-83a0-49c3585a0c77",
                    "LayerId": "bfd1faa1-5c86-4020-a22d-3e25732f6910"
                }
            ]
        },
        {
            "id": "ea5668f6-9e5d-4bb0-b08d-4fa0b3e5d715",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "37d02ee1-99cc-4ce5-8aec-f89940cc5cd3",
            "compositeImage": {
                "id": "f9866b60-2d7f-42d4-a3c4-ca0034f15776",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea5668f6-9e5d-4bb0-b08d-4fa0b3e5d715",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3ba18a5-ecac-4eaa-b8b4-6ef79123b545",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea5668f6-9e5d-4bb0-b08d-4fa0b3e5d715",
                    "LayerId": "bfd1faa1-5c86-4020-a22d-3e25732f6910"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "bfd1faa1-5c86-4020-a22d-3e25732f6910",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "37d02ee1-99cc-4ce5-8aec-f89940cc5cd3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}