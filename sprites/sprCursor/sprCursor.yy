{
    "id": "dfad4bfa-d8b8-4e11-afe5-f0598be1db83",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCursor",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1dbf75b9-5432-402c-ad6c-859c88af3c4e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dfad4bfa-d8b8-4e11-afe5-f0598be1db83",
            "compositeImage": {
                "id": "82d71a60-bab4-4324-86ff-0a40e1486571",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1dbf75b9-5432-402c-ad6c-859c88af3c4e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2733a668-0ea3-4537-8083-099e8ec2e775",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1dbf75b9-5432-402c-ad6c-859c88af3c4e",
                    "LayerId": "e8b970a4-e1e1-4247-a265-43ead7053b7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e8b970a4-e1e1-4247-a265-43ead7053b7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dfad4bfa-d8b8-4e11-afe5-f0598be1db83",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}