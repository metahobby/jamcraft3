{
    "id": "c9927f11-98ca-4705-b7b9-db52f6b34402",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprNext",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bcd9bb23-214c-4bdc-b4d5-fb30c393a11a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c9927f11-98ca-4705-b7b9-db52f6b34402",
            "compositeImage": {
                "id": "6cb729de-e3fc-4f37-8fa0-fe7a57ec46a4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bcd9bb23-214c-4bdc-b4d5-fb30c393a11a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dbef14c5-8cc8-4a5a-81e3-44fe6c180fe8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bcd9bb23-214c-4bdc-b4d5-fb30c393a11a",
                    "LayerId": "2e3517d2-3bf4-41c9-ab2a-2e3fee686302"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2e3517d2-3bf4-41c9-ab2a-2e3fee686302",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c9927f11-98ca-4705-b7b9-db52f6b34402",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}