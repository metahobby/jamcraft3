{
    "id": "a6fa89b6-7321-4cb3-b665-cff5560eb86e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprInvSlot",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ffa479c8-0209-408a-bfe1-61440a79fd37",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a6fa89b6-7321-4cb3-b665-cff5560eb86e",
            "compositeImage": {
                "id": "9147f3b1-5489-4b80-a64d-5f46c8840963",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffa479c8-0209-408a-bfe1-61440a79fd37",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "07cdd280-2ea5-45ff-8e09-ffc7f5916f35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffa479c8-0209-408a-bfe1-61440a79fd37",
                    "LayerId": "92bfc844-d5cc-4200-8386-6fb58e0daa4c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "92bfc844-d5cc-4200-8386-6fb58e0daa4c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a6fa89b6-7321-4cb3-b665-cff5560eb86e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}