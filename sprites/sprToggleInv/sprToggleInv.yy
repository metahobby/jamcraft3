{
    "id": "c00142a4-7144-449a-b3cc-a9fc1b88b83c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprToggleInv",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 47,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "798f19c4-fa4e-49ae-99e1-cc8076d4f90c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c00142a4-7144-449a-b3cc-a9fc1b88b83c",
            "compositeImage": {
                "id": "3a34341a-2cc4-4dc0-b104-903058bedeb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "798f19c4-fa4e-49ae-99e1-cc8076d4f90c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a14a5bab-eae0-4e32-b445-1c3c9ef92353",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "798f19c4-fa4e-49ae-99e1-cc8076d4f90c",
                    "LayerId": "744f25d6-2ba0-4434-b195-88e5d733a139"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "744f25d6-2ba0-4434-b195-88e5d733a139",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c00142a4-7144-449a-b3cc-a9fc1b88b83c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 10,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}