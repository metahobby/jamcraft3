{
    "id": "4f9e95b6-46b3-45ac-94a4-a58af1a3d841",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotEyesMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 25,
    "bbox_left": 66,
    "bbox_right": 76,
    "bbox_top": 20,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "93ca86e4-eeb2-4263-811c-63d6912884ef",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f9e95b6-46b3-45ac-94a4-a58af1a3d841",
            "compositeImage": {
                "id": "2decb873-ab23-4240-a200-7036157ce844",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93ca86e4-eeb2-4263-811c-63d6912884ef",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a8bc5707-b786-45af-9d37-576450188440",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93ca86e4-eeb2-4263-811c-63d6912884ef",
                    "LayerId": "785b9a3f-f066-43ad-8bcc-a38dce783448"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "785b9a3f-f066-43ad-8bcc-a38dce783448",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f9e95b6-46b3-45ac-94a4-a58af1a3d841",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}