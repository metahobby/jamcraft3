{
    "id": "73f726df-85cb-4e84-a82d-468adf933179",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSlime",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8b07087e-8604-427a-8b37-c9cdec97c7c1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "compositeImage": {
                "id": "f2ee6197-6143-4684-8966-492894858676",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8b07087e-8604-427a-8b37-c9cdec97c7c1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0d3c4d-9ad3-4b1f-9d8c-101cb97c3c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8b07087e-8604-427a-8b37-c9cdec97c7c1",
                    "LayerId": "d91e818c-90da-4afa-9186-db43b7cac003"
                }
            ]
        },
        {
            "id": "7916977c-fff4-4907-b072-fefe8c8246d4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "compositeImage": {
                "id": "5f3ba29a-e314-4ed7-9476-576c9d0cd83f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7916977c-fff4-4907-b072-fefe8c8246d4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dac5b589-0f75-46fb-ab95-79f2d505ca13",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7916977c-fff4-4907-b072-fefe8c8246d4",
                    "LayerId": "d91e818c-90da-4afa-9186-db43b7cac003"
                }
            ]
        },
        {
            "id": "6be786d0-e5e0-4519-b39b-437f5fae9b68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "compositeImage": {
                "id": "67f937a4-18a3-445b-a6cb-1418ecddeac3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6be786d0-e5e0-4519-b39b-437f5fae9b68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2182a178-f0c4-425d-9cbb-f27e83d44d10",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6be786d0-e5e0-4519-b39b-437f5fae9b68",
                    "LayerId": "d91e818c-90da-4afa-9186-db43b7cac003"
                }
            ]
        },
        {
            "id": "025e8e75-3561-4157-b045-67e3632de875",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "compositeImage": {
                "id": "e0b3579e-9f85-4901-9a38-f761d773c8c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "025e8e75-3561-4157-b045-67e3632de875",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5d46c9-3fc8-43e1-92a8-b944a8972fa2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "025e8e75-3561-4157-b045-67e3632de875",
                    "LayerId": "d91e818c-90da-4afa-9186-db43b7cac003"
                }
            ]
        },
        {
            "id": "5680604f-3c96-4ed5-be53-292f729941bc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "compositeImage": {
                "id": "7dbe6c0a-0433-4f41-9403-b3d74426cc3d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5680604f-3c96-4ed5-be53-292f729941bc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98a23e10-1872-4ba3-8203-35326d8b839d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5680604f-3c96-4ed5-be53-292f729941bc",
                    "LayerId": "d91e818c-90da-4afa-9186-db43b7cac003"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d91e818c-90da-4afa-9186-db43b7cac003",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73f726df-85cb-4e84-a82d-468adf933179",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}