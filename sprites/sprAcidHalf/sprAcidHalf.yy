{
    "id": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprAcidHalf",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "72baac8b-7566-4727-ae6f-7bfbce6e610b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "dac4a733-eb55-4ce4-baf7-15b205989981",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72baac8b-7566-4727-ae6f-7bfbce6e610b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "83752311-5178-41f2-8a63-eedfce356bf0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72baac8b-7566-4727-ae6f-7bfbce6e610b",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "31758bd4-914d-4189-a36f-1711f956533b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "f08c27f8-7a1b-4d7f-80f0-6bb97d7a6a03",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "31758bd4-914d-4189-a36f-1711f956533b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "71c95b97-71a6-4e55-9883-072c23d70c64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "31758bd4-914d-4189-a36f-1711f956533b",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "2413ded5-a6cf-4dc5-ab19-14f01e826524",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "aefd412c-b586-48a2-96d8-d1e154341250",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2413ded5-a6cf-4dc5-ab19-14f01e826524",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29cfab93-2a2a-4afc-8540-4a6e6cdbcffe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2413ded5-a6cf-4dc5-ab19-14f01e826524",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "0a338d53-d782-445d-819e-9103a133051f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "88d2f98e-4940-41e8-9e4f-7c95aab8bb3b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a338d53-d782-445d-819e-9103a133051f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45e237d1-af25-4e65-bf7d-7ff5b80aae1f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a338d53-d782-445d-819e-9103a133051f",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "5b84eb91-2236-4fc9-bb52-ef32b130d48b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "53dbdf20-4ef9-48e1-9bc1-3db41b72220a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b84eb91-2236-4fc9-bb52-ef32b130d48b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "24428bcd-c894-463f-a207-9a686dd951f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b84eb91-2236-4fc9-bb52-ef32b130d48b",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "cbadc8e2-c3c0-4e39-8d39-fcb922ad8bfe",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "b0359927-4b2d-4682-99c4-0167f5f43b97",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbadc8e2-c3c0-4e39-8d39-fcb922ad8bfe",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "09dd8b43-04a2-4722-aca6-fefbd985940c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbadc8e2-c3c0-4e39-8d39-fcb922ad8bfe",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "54e5fea4-f859-4e42-9bbf-f85e140e8e5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "18412f25-49db-40d6-bd79-41dfde3884be",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54e5fea4-f859-4e42-9bbf-f85e140e8e5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "badbe717-2e08-4be5-9d0d-1dde74db149d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54e5fea4-f859-4e42-9bbf-f85e140e8e5b",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "2402b492-5936-469d-b4f9-4db6b7db454a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "c08a431f-9c91-4d1b-9c49-b8f6d89bd6df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2402b492-5936-469d-b4f9-4db6b7db454a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bfbd373-6b47-4e90-994a-3b72f100f737",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2402b492-5936-469d-b4f9-4db6b7db454a",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "0a3c4975-c2e5-45e9-8f31-74eae6d047ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "bb83b2d3-9b5a-45a3-832d-d9edbc3c853c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a3c4975-c2e5-45e9-8f31-74eae6d047ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5b73b45-0d23-4d95-b247-0e085f9c6364",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a3c4975-c2e5-45e9-8f31-74eae6d047ad",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "448288f2-79e0-44bc-b8de-4ba9e3e93dfc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "18e6d136-b049-46ee-9588-b48d722403b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "448288f2-79e0-44bc-b8de-4ba9e3e93dfc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "268a2b0e-12c2-4c28-89b6-7f2231d3c323",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "448288f2-79e0-44bc-b8de-4ba9e3e93dfc",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "f3b16a80-36a0-4823-b63f-c9da17497ce3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "e6dfbc6d-9e9e-4e8e-9697-0cf70766975b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f3b16a80-36a0-4823-b63f-c9da17497ce3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "576032a7-9516-4eca-9040-1c7aa6557bdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f3b16a80-36a0-4823-b63f-c9da17497ce3",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "67a603ef-e59a-4d1d-9318-ca7aa69cfe43",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "f92220a3-ebba-4c50-a7de-68b47e467825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67a603ef-e59a-4d1d-9318-ca7aa69cfe43",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7ea9f9cb-7e24-4055-b175-ea921d30301d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67a603ef-e59a-4d1d-9318-ca7aa69cfe43",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "4937b47b-95c4-4e52-9e24-d6cedef7a9d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "a2e73c67-17f6-4553-82c0-2656f5bbb79d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4937b47b-95c4-4e52-9e24-d6cedef7a9d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0bb8c0ec-f661-4560-8867-92606e5ac9f7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4937b47b-95c4-4e52-9e24-d6cedef7a9d0",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "7aef9a46-2c19-40de-a771-6089122c5ae7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "e05006e6-704b-46da-a2ad-4fcf86271206",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7aef9a46-2c19-40de-a771-6089122c5ae7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4a394110-9279-493e-86d4-a7b52238e37d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7aef9a46-2c19-40de-a771-6089122c5ae7",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "01ad9a8e-726d-4496-bd76-013a59f668fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "369fea8d-8a13-457f-aa37-7aedcfac100e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "01ad9a8e-726d-4496-bd76-013a59f668fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ca5bbd60-0c26-4c94-93d7-d879696061d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "01ad9a8e-726d-4496-bd76-013a59f668fc",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        },
        {
            "id": "2860d4d3-dc92-47f3-a137-f776a688b62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "compositeImage": {
                "id": "c92e6331-5343-4a43-ba84-22a7a251e567",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2860d4d3-dc92-47f3-a137-f776a688b62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "18ecb5ce-0628-44f5-aac7-d373bef00c7b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2860d4d3-dc92-47f3-a137-f776a688b62b",
                    "LayerId": "004f5bca-4c3a-4afc-af03-8928cff04d20"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "004f5bca-4c3a-4afc-af03-8928cff04d20",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ecdd5ad1-5178-44a8-a856-acfcdf78d078",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}