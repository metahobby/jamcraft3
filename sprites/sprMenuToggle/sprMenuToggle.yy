{
    "id": "f01a5479-de3c-4409-9d47-9dab612f56b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprMenuToggle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 126,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "13533c91-9871-4163-8157-7672f75903ff",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "f01a5479-de3c-4409-9d47-9dab612f56b1",
            "compositeImage": {
                "id": "9c940d21-fe98-4e14-98b3-5d82dc7a311f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "13533c91-9871-4163-8157-7672f75903ff",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2473a5cd-4094-42d6-af13-a3d49af1ad64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "13533c91-9871-4163-8157-7672f75903ff",
                    "LayerId": "b7d3ab39-9d3d-444f-bd53-64da54d127fd"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "b7d3ab39-9d3d-444f-bd53-64da54d127fd",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "f01a5479-de3c-4409-9d47-9dab612f56b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 8
}