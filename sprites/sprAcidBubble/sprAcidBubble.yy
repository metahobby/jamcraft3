{
    "id": "e6a5592d-6e3c-4f67-8e11-64d2944f5776",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprAcidBubble",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 27,
    "bbox_left": 4,
    "bbox_right": 27,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e7ec6dba-cfdb-4ae5-8475-486f461152c8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6a5592d-6e3c-4f67-8e11-64d2944f5776",
            "compositeImage": {
                "id": "fcee9877-c5c5-4fad-b96c-75bfd7c3ff07",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e7ec6dba-cfdb-4ae5-8475-486f461152c8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bb1549f5-307d-4cb7-9f8d-440a7cef5918",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e7ec6dba-cfdb-4ae5-8475-486f461152c8",
                    "LayerId": "f85183b3-9b72-4f0e-a708-ac7d662ed25a"
                }
            ]
        },
        {
            "id": "1a5dbdf6-4d66-4a19-9abe-aa8e1d5a12b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e6a5592d-6e3c-4f67-8e11-64d2944f5776",
            "compositeImage": {
                "id": "532420ec-ff84-414d-a2a9-cc36998693c3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1a5dbdf6-4d66-4a19-9abe-aa8e1d5a12b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "67bce5f1-eb3a-41f0-94f4-40827253ece7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1a5dbdf6-4d66-4a19-9abe-aa8e1d5a12b5",
                    "LayerId": "f85183b3-9b72-4f0e-a708-ac7d662ed25a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f85183b3-9b72-4f0e-a708-ac7d662ed25a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e6a5592d-6e3c-4f67-8e11-64d2944f5776",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}