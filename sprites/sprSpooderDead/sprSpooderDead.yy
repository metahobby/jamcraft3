{
    "id": "e8137386-beab-46d9-a34c-c8a5593d04e5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpooderDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "02c93ff5-e177-4976-9f67-dcb11d1540a7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8137386-beab-46d9-a34c-c8a5593d04e5",
            "compositeImage": {
                "id": "a6a97079-4624-43db-928e-da59ffd783d5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "02c93ff5-e177-4976-9f67-dcb11d1540a7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c8e93d72-5e0a-4f43-844b-2cd9b48bb122",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "02c93ff5-e177-4976-9f67-dcb11d1540a7",
                    "LayerId": "e4739c23-a5a8-4b22-a9c9-54359e7e8735"
                }
            ]
        },
        {
            "id": "51c3081b-4cd7-45c5-9b0f-09769cb400af",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8137386-beab-46d9-a34c-c8a5593d04e5",
            "compositeImage": {
                "id": "8d394048-71a5-41a5-b0ff-ee6d6f52bf96",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51c3081b-4cd7-45c5-9b0f-09769cb400af",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b99dee4c-f325-4c13-b9db-57ad596cec64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51c3081b-4cd7-45c5-9b0f-09769cb400af",
                    "LayerId": "e4739c23-a5a8-4b22-a9c9-54359e7e8735"
                }
            ]
        },
        {
            "id": "d4bbb682-91de-44cc-9631-3199dc2ede4a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8137386-beab-46d9-a34c-c8a5593d04e5",
            "compositeImage": {
                "id": "68ead0b4-5077-4ce6-8692-796dc5b86b37",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d4bbb682-91de-44cc-9631-3199dc2ede4a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8b3d7098-d2ff-4f80-ba2b-e3f7ac6f0714",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d4bbb682-91de-44cc-9631-3199dc2ede4a",
                    "LayerId": "e4739c23-a5a8-4b22-a9c9-54359e7e8735"
                }
            ]
        },
        {
            "id": "37b39547-26a0-4c16-9b09-16f04fd95f5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e8137386-beab-46d9-a34c-c8a5593d04e5",
            "compositeImage": {
                "id": "55e38326-e5f0-445a-9674-0dc782887791",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b39547-26a0-4c16-9b09-16f04fd95f5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "89e34efb-acf3-4167-8b94-dfd0daff7b0c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b39547-26a0-4c16-9b09-16f04fd95f5e",
                    "LayerId": "e4739c23-a5a8-4b22-a9c9-54359e7e8735"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e4739c23-a5a8-4b22-a9c9-54359e7e8735",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e8137386-beab-46d9-a34c-c8a5593d04e5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}