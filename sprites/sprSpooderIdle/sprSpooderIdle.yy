{
    "id": "c6925877-8b16-45bd-bfb3-f87739fbbb41",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpooderIdle",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 30,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5b39b6b4-9d52-48f8-855e-6afdc5e67322",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6925877-8b16-45bd-bfb3-f87739fbbb41",
            "compositeImage": {
                "id": "789e8af7-5589-4a38-a4e2-ba11e0631fa8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5b39b6b4-9d52-48f8-855e-6afdc5e67322",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb745e0-cd5e-4609-895e-0e93f5af6f9b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5b39b6b4-9d52-48f8-855e-6afdc5e67322",
                    "LayerId": "d059cdcb-3ded-48ff-9670-76b3e96c7646"
                }
            ]
        },
        {
            "id": "4223e45a-db8f-4dd6-af2b-bf158ad2d5a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c6925877-8b16-45bd-bfb3-f87739fbbb41",
            "compositeImage": {
                "id": "c174b882-e421-4d30-8c4b-8ec7001e79de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4223e45a-db8f-4dd6-af2b-bf158ad2d5a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1666e2cd-8cc9-4b64-b834-6eb11e9bc719",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4223e45a-db8f-4dd6-af2b-bf158ad2d5a9",
                    "LayerId": "d059cdcb-3ded-48ff-9670-76b3e96c7646"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d059cdcb-3ded-48ff-9670-76b3e96c7646",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c6925877-8b16-45bd-bfb3-f87739fbbb41",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}