{
    "id": "c25bcec3-dfae-4209-8bee-f37dd1e68eb6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprPixel",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "58276b22-ada9-477f-a091-245f4215ddd9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c25bcec3-dfae-4209-8bee-f37dd1e68eb6",
            "compositeImage": {
                "id": "8c8cf1a0-df1c-441e-8d0e-c3d24621df2f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "58276b22-ada9-477f-a091-245f4215ddd9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9df55cd-44ac-4211-b6a0-64e7ab273bcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "58276b22-ada9-477f-a091-245f4215ddd9",
                    "LayerId": "9590e0dc-9a4c-499a-a2c0-c895bb4a41c0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 1,
    "layers": [
        {
            "id": "9590e0dc-9a4c-499a-a2c0-c895bb4a41c0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c25bcec3-dfae-4209-8bee-f37dd1e68eb6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 1,
    "xorig": 0,
    "yorig": 0
}