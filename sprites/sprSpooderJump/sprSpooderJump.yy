{
    "id": "32371b7d-07fe-499a-bb43-6559aecd44d3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprSpooderJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 3,
    "bbox_right": 28,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "60528f39-ec5d-4f2c-8b4f-edb21b932727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "32371b7d-07fe-499a-bb43-6559aecd44d3",
            "compositeImage": {
                "id": "08a8c80e-f399-447e-a2bc-b7d8e73b4f3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "60528f39-ec5d-4f2c-8b4f-edb21b932727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff9fe861-475c-41c5-be3a-1a5c760a970c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "60528f39-ec5d-4f2c-8b4f-edb21b932727",
                    "LayerId": "a8e5e4a3-6810-426b-a9f4-37b7520e85d6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "a8e5e4a3-6810-426b-a9f4-37b7520e85d6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "32371b7d-07fe-499a-bb43-6559aecd44d3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": true,
    "playbackSpeed": 1,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 31
}