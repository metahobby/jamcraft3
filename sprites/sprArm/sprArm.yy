{
    "id": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprArm",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 48,
    "bbox_left": 0,
    "bbox_right": 48,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "927f11dd-62c1-4045-bce5-3b8b69c9b187",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "compositeImage": {
                "id": "513ae056-332c-4d87-8c5b-982dbb0abbb4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "927f11dd-62c1-4045-bce5-3b8b69c9b187",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c282853e-6194-4bd6-935d-199584124330",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "927f11dd-62c1-4045-bce5-3b8b69c9b187",
                    "LayerId": "43ab41d8-6061-4435-8baa-4ab29d54c7f9"
                }
            ]
        },
        {
            "id": "c7887722-afb8-47f0-9d24-0abd9be8c542",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "compositeImage": {
                "id": "86b0c65b-03e2-4ab6-8c7f-a33c740f7469",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7887722-afb8-47f0-9d24-0abd9be8c542",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d7ca9b4-2cf7-4b53-92ba-e9ed6498181d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7887722-afb8-47f0-9d24-0abd9be8c542",
                    "LayerId": "43ab41d8-6061-4435-8baa-4ab29d54c7f9"
                }
            ]
        },
        {
            "id": "4d5a82d2-1952-4131-8641-bc973822d8bf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "compositeImage": {
                "id": "3d5e246e-0c70-44a1-b49f-558a083baf36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4d5a82d2-1952-4131-8641-bc973822d8bf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2a17b6d0-4811-469a-a408-68bfc69a64ed",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4d5a82d2-1952-4131-8641-bc973822d8bf",
                    "LayerId": "43ab41d8-6061-4435-8baa-4ab29d54c7f9"
                }
            ]
        },
        {
            "id": "819b7fc3-d9a8-4bff-b08c-c3a43254ba39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "compositeImage": {
                "id": "c6b5d485-3300-4d98-990b-dfdf41409669",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "819b7fc3-d9a8-4bff-b08c-c3a43254ba39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ba43348f-00ab-4d94-a001-fd264a953ccd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "819b7fc3-d9a8-4bff-b08c-c3a43254ba39",
                    "LayerId": "43ab41d8-6061-4435-8baa-4ab29d54c7f9"
                }
            ]
        },
        {
            "id": "bfe751a4-899a-444f-b7d4-da7f87ac5c08",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "compositeImage": {
                "id": "b3583cc8-b237-438f-9259-585c4a4577e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bfe751a4-899a-444f-b7d4-da7f87ac5c08",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "08683bd2-9eee-42de-a7d9-4f97dd4debfe",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bfe751a4-899a-444f-b7d4-da7f87ac5c08",
                    "LayerId": "43ab41d8-6061-4435-8baa-4ab29d54c7f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "43ab41d8-6061-4435-8baa-4ab29d54c7f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27f9e8f1-6d17-4562-a4bc-f61f2c8f4a5b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 38
}