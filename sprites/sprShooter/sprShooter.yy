{
    "id": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprShooter",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e757cbad-0a42-41f8-8f8c-6e44802c3042",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "b4097c53-40f7-496c-b9c2-40bdb01f55da",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e757cbad-0a42-41f8-8f8c-6e44802c3042",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "961b8794-d087-4965-90cd-a1857978f187",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e757cbad-0a42-41f8-8f8c-6e44802c3042",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "cee1be71-0816-486e-be8b-c0baa4dda371",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "4f02047a-9db4-4ea8-be4b-0f794921e828",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee1be71-0816-486e-be8b-c0baa4dda371",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a4c80a74-85cb-4c7f-a931-8e6f60918cd7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee1be71-0816-486e-be8b-c0baa4dda371",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "a8207d98-0682-4ec4-98e9-a791911ff77a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "7f9a23aa-4b24-4f42-96be-eabfa36380cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8207d98-0682-4ec4-98e9-a791911ff77a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0dadf5e-2273-49e6-b6d5-9768998f5097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8207d98-0682-4ec4-98e9-a791911ff77a",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "c246960a-ad8a-456c-85ce-65b06566cb3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "978e27bd-8a4f-473c-aa3c-645e5be5166e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c246960a-ad8a-456c-85ce-65b06566cb3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ae3cdce1-5b5c-4311-a1fa-80908561475f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c246960a-ad8a-456c-85ce-65b06566cb3f",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "f5c5d637-91e8-4c2d-a0e2-50c52e6fa7d0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "3296e517-eef6-4e68-a031-d12f6885dc4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5c5d637-91e8-4c2d-a0e2-50c52e6fa7d0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddde5c0c-d03b-413d-a939-61c426db7ee2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5c5d637-91e8-4c2d-a0e2-50c52e6fa7d0",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "0a323df9-1aed-4687-89fd-b3bbbdb1d78a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "0fb1b9e1-7fbc-4a1a-952d-d370beb598d2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0a323df9-1aed-4687-89fd-b3bbbdb1d78a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7369c389-c1e5-4bc6-a24f-e0dda79a4e3b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0a323df9-1aed-4687-89fd-b3bbbdb1d78a",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "97a570d6-0270-4fab-9c8a-4fd7980f4055",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "2871afb4-bf85-4347-bb47-f51ca812c8b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "97a570d6-0270-4fab-9c8a-4fd7980f4055",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8bcab80d-0412-4a93-886a-dd57e3720538",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "97a570d6-0270-4fab-9c8a-4fd7980f4055",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "f0836574-1fca-4b99-821f-25451b518db9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "2c621218-2c66-48a7-b3f4-1760b30e370a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0836574-1fca-4b99-821f-25451b518db9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1acae1d3-1f86-4fbc-a8da-0579ef3b52db",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0836574-1fca-4b99-821f-25451b518db9",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "bca6b8bd-bea1-446a-a1a9-149b890c474f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "d48a88bb-0a21-499f-9f4f-a72d2a190fbf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bca6b8bd-bea1-446a-a1a9-149b890c474f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85d6613e-4fa1-414f-8f8a-f1676f52fdf7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bca6b8bd-bea1-446a-a1a9-149b890c474f",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        },
        {
            "id": "2d39ffee-a5a0-4041-b7f8-ca454c8f210d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "compositeImage": {
                "id": "2e3d0254-505b-4371-bc49-9392d88fcab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2d39ffee-a5a0-4041-b7f8-ca454c8f210d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "60436bf0-c55d-4b09-93a4-3892d2954bd5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2d39ffee-a5a0-4041-b7f8-ca454c8f210d",
                    "LayerId": "de5ace50-0379-4010-807a-32cad58b3202"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "de5ace50-0379-4010-807a-32cad58b3202",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a8fbca1b-9fc1-4a3f-88c5-5596397b9b71",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 4,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}