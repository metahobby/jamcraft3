{
    "id": "67bd8606-e142-4c93-8a94-7261034d15be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotVison",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 151,
    "bbox_left": 0,
    "bbox_right": 209,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "44468320-bb43-4f88-b1c2-1b3e4e996ae9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "67bd8606-e142-4c93-8a94-7261034d15be",
            "compositeImage": {
                "id": "85c009e3-35a8-42a0-9527-781bd8762060",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "44468320-bb43-4f88-b1c2-1b3e4e996ae9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fa6d23c2-51cb-4964-b648-f98cde61e95d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "44468320-bb43-4f88-b1c2-1b3e4e996ae9",
                    "LayerId": "544b6d10-1981-4fd0-baa7-214903c54ae5"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 152,
    "layers": [
        {
            "id": "544b6d10-1981-4fd0-baa7-214903c54ae5",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "67bd8606-e142-4c93-8a94-7261034d15be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 3,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 210,
    "xorig": 0,
    "yorig": 76
}