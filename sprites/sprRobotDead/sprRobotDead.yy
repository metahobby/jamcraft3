{
    "id": "12b1fd96-942e-4d55-9646-b2a949cc7b9f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotDead",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 14,
    "bbox_right": 83,
    "bbox_top": 16,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ea813da3-d48f-49c3-9302-01e1a2ef5522",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b1fd96-942e-4d55-9646-b2a949cc7b9f",
            "compositeImage": {
                "id": "019c87a2-eab7-4738-ab49-7307a78c4c8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ea813da3-d48f-49c3-9302-01e1a2ef5522",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5139aeef-8543-434e-9aed-fef9f7292f02",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ea813da3-d48f-49c3-9302-01e1a2ef5522",
                    "LayerId": "9262a53d-6f20-4cb7-9aa6-fb61264498d3"
                }
            ]
        },
        {
            "id": "b7e634a6-f930-4454-b88f-fe240740ed6e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "12b1fd96-942e-4d55-9646-b2a949cc7b9f",
            "compositeImage": {
                "id": "3a76ea1a-9348-4f1a-867e-145f6e3c608c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b7e634a6-f930-4454-b88f-fe240740ed6e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4fe9e2dc-95dc-4a2b-9fce-c42625e2e137",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b7e634a6-f930-4454-b88f-fe240740ed6e",
                    "LayerId": "9262a53d-6f20-4cb7-9aa6-fb61264498d3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "9262a53d-6f20-4cb7-9aa6-fb61264498d3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "12b1fd96-942e-4d55-9646-b2a949cc7b9f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}