{
    "id": "90f0b151-c0fe-43ad-b6b1-9236ef557c15",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotPanelMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 53,
    "bbox_left": 30,
    "bbox_right": 47,
    "bbox_top": 42,
    "bboxmode": 0,
    "colkind": 0,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c99da8cd-ec32-404f-a805-a7b07b9f0b58",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "90f0b151-c0fe-43ad-b6b1-9236ef557c15",
            "compositeImage": {
                "id": "b89f3022-262f-4054-8280-d86e3cf5df39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c99da8cd-ec32-404f-a805-a7b07b9f0b58",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7c0aa442-d81c-47dd-ba71-cb622a9be2f1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c99da8cd-ec32-404f-a805-a7b07b9f0b58",
                    "LayerId": "3b776aec-6c0b-4424-863b-3bc71dd67833"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 76,
    "layers": [
        {
            "id": "3b776aec-6c0b-4424-863b-3bc71dd67833",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "90f0b151-c0fe-43ad-b6b1-9236ef557c15",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 7,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}