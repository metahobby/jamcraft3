{
    "id": "979f9bb1-271b-47ff-9ca3-c93187475f22",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprNSlice",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 23,
    "bbox_left": 0,
    "bbox_right": 23,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2586cc3-57ee-48a3-886b-4a1d28fea35e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "979f9bb1-271b-47ff-9ca3-c93187475f22",
            "compositeImage": {
                "id": "5156e209-e9d5-4c83-ae5d-136a4f419b28",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2586cc3-57ee-48a3-886b-4a1d28fea35e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c5d2646-8c2b-49aa-88c0-1505548ab678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2586cc3-57ee-48a3-886b-4a1d28fea35e",
                    "LayerId": "6473ee37-b2ab-482a-80fb-d47d516f9405"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 24,
    "layers": [
        {
            "id": "6473ee37-b2ab-482a-80fb-d47d516f9405",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "979f9bb1-271b-47ff-9ca3-c93187475f22",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 24,
    "xorig": 0,
    "yorig": 0
}