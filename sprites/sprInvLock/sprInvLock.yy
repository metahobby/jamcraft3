{
    "id": "3ba569bf-7f1b-4a03-a37b-3bf4a7ab3cc4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprInvLock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3669bd6a-8c4b-4a40-a1d0-dcbfffa74579",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3ba569bf-7f1b-4a03-a37b-3bf4a7ab3cc4",
            "compositeImage": {
                "id": "90dfba4c-bb02-4db4-a863-9d1daff8f73b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3669bd6a-8c4b-4a40-a1d0-dcbfffa74579",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c996f147-b81c-4000-bdbc-f232f476eec1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3669bd6a-8c4b-4a40-a1d0-dcbfffa74579",
                    "LayerId": "92169caa-2403-4346-baba-565c99f99ed2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "92169caa-2403-4346-baba-565c99f99ed2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3ba569bf-7f1b-4a03-a37b-3bf4a7ab3cc4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}