{
    "id": "738dd53b-e963-4adf-b1a1-68696861c427",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotJump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 14,
    "bbox_right": 84,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "074a17ea-d193-4e6c-9bb1-67b7487c1ecd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "compositeImage": {
                "id": "4fb2b2f8-d373-45c1-b307-62c46c022c45",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "074a17ea-d193-4e6c-9bb1-67b7487c1ecd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f58824a8-8ef9-4119-91ec-45f9e494f3bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "074a17ea-d193-4e6c-9bb1-67b7487c1ecd",
                    "LayerId": "f95f43d7-5368-4883-b3ce-51ebbbc7195c"
                }
            ]
        },
        {
            "id": "038b2551-9192-414a-b568-5e9955328e2f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "compositeImage": {
                "id": "162adfa2-ba69-43ce-aa48-d8b248b733a8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "038b2551-9192-414a-b568-5e9955328e2f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a5f6ae38-f609-4391-bb8c-287c007865a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "038b2551-9192-414a-b568-5e9955328e2f",
                    "LayerId": "f95f43d7-5368-4883-b3ce-51ebbbc7195c"
                }
            ]
        },
        {
            "id": "3479a818-b62e-4abc-b84b-aaa3adbf02b3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "compositeImage": {
                "id": "a77be607-0577-4ef7-93a3-408e570a90b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3479a818-b62e-4abc-b84b-aaa3adbf02b3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38a7fdd7-9e70-4f9e-9c90-0ca9f6d8912d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3479a818-b62e-4abc-b84b-aaa3adbf02b3",
                    "LayerId": "f95f43d7-5368-4883-b3ce-51ebbbc7195c"
                }
            ]
        },
        {
            "id": "e5e9370f-a01c-47a5-86f1-22175909501c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "compositeImage": {
                "id": "507887f3-9785-4051-8317-2b3ccc32787e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5e9370f-a01c-47a5-86f1-22175909501c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f33765d-532f-4234-8ade-7b182cddd76a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5e9370f-a01c-47a5-86f1-22175909501c",
                    "LayerId": "f95f43d7-5368-4883-b3ce-51ebbbc7195c"
                }
            ]
        },
        {
            "id": "623a2cea-a153-47dd-a42a-25b0b84dc62b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "compositeImage": {
                "id": "b56f1ddb-4d51-4402-ac83-463de072e3ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "623a2cea-a153-47dd-a42a-25b0b84dc62b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "efb3247c-449c-4eb7-ad9a-0fd29bf8a3d1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "623a2cea-a153-47dd-a42a-25b0b84dc62b",
                    "LayerId": "f95f43d7-5368-4883-b3ce-51ebbbc7195c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "f95f43d7-5368-4883-b3ce-51ebbbc7195c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "738dd53b-e963-4adf-b1a1-68696861c427",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}