{
    "id": "c0585c60-51d3-4a86-9d6f-417b41fb61f6",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprBG",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7ca2a883-28fe-4d02-a3f8-5187c7770268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c0585c60-51d3-4a86-9d6f-417b41fb61f6",
            "compositeImage": {
                "id": "415155f9-36c0-4050-bcb3-48c780cd16b7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7ca2a883-28fe-4d02-a3f8-5187c7770268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d73027f8-f03a-4cf3-a058-839605f08b25",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7ca2a883-28fe-4d02-a3f8-5187c7770268",
                    "LayerId": "29f7d67e-dc03-4666-b168-83249b4df454"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "29f7d67e-dc03-4666-b168-83249b4df454",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c0585c60-51d3-4a86-9d6f-417b41fb61f6",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}