{
    "id": "4d649227-fdde-4182-a0e2-65188ba607f3",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprRobotJumpMask",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 75,
    "bbox_left": 14,
    "bbox_right": 84,
    "bbox_top": 16,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bbd6e743-6f06-4d60-a17d-fef6150b5625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "compositeImage": {
                "id": "4d84d1fc-9e91-4a76-b3b1-3c44c2dcdaec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbd6e743-6f06-4d60-a17d-fef6150b5625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a96a10bf-82d4-49a6-942f-91fd59caed1c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbd6e743-6f06-4d60-a17d-fef6150b5625",
                    "LayerId": "9232b953-41b3-4cd2-bed0-5a2604a80e8b"
                }
            ]
        },
        {
            "id": "fad8593d-b54c-41e8-b7ab-2ea3c16430c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "compositeImage": {
                "id": "78ff060b-4fe6-4435-bf92-b1edc37060d9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fad8593d-b54c-41e8-b7ab-2ea3c16430c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "648e9df3-6a9a-43e2-b2db-590e2c463355",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fad8593d-b54c-41e8-b7ab-2ea3c16430c9",
                    "LayerId": "9232b953-41b3-4cd2-bed0-5a2604a80e8b"
                }
            ]
        },
        {
            "id": "4088045f-f0bf-4a84-a37f-fb181565dc21",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "compositeImage": {
                "id": "f9fefd1a-3893-4d12-9712-f10336847f75",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4088045f-f0bf-4a84-a37f-fb181565dc21",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "98b0b500-a1fa-43f8-a5b5-62d148ea37bc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4088045f-f0bf-4a84-a37f-fb181565dc21",
                    "LayerId": "9232b953-41b3-4cd2-bed0-5a2604a80e8b"
                }
            ]
        },
        {
            "id": "75971096-cd69-451e-a24c-db5389507830",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "compositeImage": {
                "id": "1e5762a8-8d8d-4b51-a8b2-145a1f8fbd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "75971096-cd69-451e-a24c-db5389507830",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb73ec98-0eff-4af1-9b85-7960b59f48fc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "75971096-cd69-451e-a24c-db5389507830",
                    "LayerId": "9232b953-41b3-4cd2-bed0-5a2604a80e8b"
                }
            ]
        },
        {
            "id": "df62fd89-51a2-47dd-943f-c1e521d42d5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "compositeImage": {
                "id": "e288fdff-34ba-4a79-9a81-28027275969b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "df62fd89-51a2-47dd-943f-c1e521d42d5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0c32ab7a-738f-490d-b588-a0df71b50e72",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "df62fd89-51a2-47dd-943f-c1e521d42d5d",
                    "LayerId": "9232b953-41b3-4cd2-bed0-5a2604a80e8b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "9232b953-41b3-4cd2-bed0-5a2604a80e8b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d649227-fdde-4182-a0e2-65188ba607f3",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 84,
    "xorig": 42,
    "yorig": 75
}