{
    "id": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprAcidPit",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 11,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b2e17dc4-9d5b-4435-91cd-bce3df9095df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "5691e861-9dcb-4bcd-b05f-638875b1016c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2e17dc4-9d5b-4435-91cd-bce3df9095df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e638cfac-91f2-4f29-abb0-182133e25bea",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2e17dc4-9d5b-4435-91cd-bce3df9095df",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "55f7a29e-9e53-4a4b-9011-ac594edab0c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "f4b46cb3-24a5-43d6-89b2-804d6a4934b3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55f7a29e-9e53-4a4b-9011-ac594edab0c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c6a1b71-12c6-432c-a6c1-6b966f017244",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55f7a29e-9e53-4a4b-9011-ac594edab0c9",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "f07e50e2-70f7-4be6-a4ba-df00f8bd4bdb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "2e22ed5f-8850-4dbe-9ecf-dc5e37b5a94f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f07e50e2-70f7-4be6-a4ba-df00f8bd4bdb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c386c309-18aa-44f7-a14d-fb6badf38708",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f07e50e2-70f7-4be6-a4ba-df00f8bd4bdb",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "e27340c2-9a44-4778-8281-a031149a5efa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "3db45ca8-355b-4e7e-b7fc-39b428dfd13d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e27340c2-9a44-4778-8281-a031149a5efa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a69edd65-8627-49ca-abfb-be6ffaa41e65",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e27340c2-9a44-4778-8281-a031149a5efa",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "59eebfa5-497e-476d-ac35-7bda83572194",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "2dca1630-b10c-4caa-8118-bf174e0d234a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "59eebfa5-497e-476d-ac35-7bda83572194",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c2e85c39-f37a-4a2e-9ff6-4fa40ae92c22",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "59eebfa5-497e-476d-ac35-7bda83572194",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "8cd37684-ccf9-408b-92b1-42b2514e1b2c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "b6c7e853-408a-4218-88e2-37221a42dd36",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8cd37684-ccf9-408b-92b1-42b2514e1b2c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "146e31e7-83ba-4ed5-9aaf-32da479942e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8cd37684-ccf9-408b-92b1-42b2514e1b2c",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "094579ff-4eaf-463d-a7ba-23260018fdf3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "eaf1326f-ac7e-4fa9-b6d6-f5b93483fa33",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "094579ff-4eaf-463d-a7ba-23260018fdf3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0758b0f1-7987-43a3-ba80-cc744cc04c41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "094579ff-4eaf-463d-a7ba-23260018fdf3",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "35fe0c06-fec0-497c-bb6d-0173dee82455",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "a5310db7-10e2-4a1a-9eef-72128b04669d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35fe0c06-fec0-497c-bb6d-0173dee82455",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cd425847-6bed-4ac7-9f76-23a82da2d7b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35fe0c06-fec0-497c-bb6d-0173dee82455",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "edc113bf-0323-412c-981f-4553c92a59b9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "5308383a-eeb6-4ebb-9774-e5dcb02302a0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edc113bf-0323-412c-981f-4553c92a59b9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5d6f02c5-98b8-4f3c-8343-df9567210289",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edc113bf-0323-412c-981f-4553c92a59b9",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "c4d458af-8327-4d8b-a2e8-74320d88ee90",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "e04c5cdb-5f69-44dd-82ab-3925dc1f83d6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4d458af-8327-4d8b-a2e8-74320d88ee90",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c24523df-6af9-4eb5-aa6f-989b3d83136b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4d458af-8327-4d8b-a2e8-74320d88ee90",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "93fbf28c-9c6c-4c50-b102-4915881aa71d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "54a07d77-10f9-4d9e-b0f4-759f66a38ad9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "93fbf28c-9c6c-4c50-b102-4915881aa71d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "68592a2e-5cbe-4c47-8c6d-4b08672a4466",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "93fbf28c-9c6c-4c50-b102-4915881aa71d",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "40327857-b21f-433c-b800-c499e9be5c00",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "ae8aa516-e5a1-4cba-87d5-d3d860813839",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40327857-b21f-433c-b800-c499e9be5c00",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c3878210-8a6b-4137-82fb-34ef4e246916",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40327857-b21f-433c-b800-c499e9be5c00",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "b5eeda83-f534-4100-ad1d-8b321a24ca6c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "445f5326-91f5-42a0-993d-388b41df49fa",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5eeda83-f534-4100-ad1d-8b321a24ca6c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "9c9834b9-8a42-469f-bd53-668efadd09bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5eeda83-f534-4100-ad1d-8b321a24ca6c",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "d96fefb1-55a9-43a9-b891-846b938fb40f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "785e7927-222f-4fe5-8659-469bcaea40ef",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d96fefb1-55a9-43a9-b891-846b938fb40f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00c334de-35b2-423e-8dfd-610d0758cef7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d96fefb1-55a9-43a9-b891-846b938fb40f",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "875de72b-5e5c-4e67-aa26-53c3ffcb9c63",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "a804a062-7041-4387-bed8-d1221393b097",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "875de72b-5e5c-4e67-aa26-53c3ffcb9c63",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "acba33fd-1df4-4950-9cb3-4924e1c80c7e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "875de72b-5e5c-4e67-aa26-53c3ffcb9c63",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        },
        {
            "id": "048b9519-d647-4fb3-8bd7-9e754f711afc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "compositeImage": {
                "id": "75b22450-d74f-4603-8061-1c0db507cd39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "048b9519-d647-4fb3-8bd7-9e754f711afc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1d0783e3-f8d2-4cdd-b904-be9df0c683a4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "048b9519-d647-4fb3-8bd7-9e754f711afc",
                    "LayerId": "64826ea9-cb06-4432-8fee-b6ff369a8c86"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "64826ea9-cb06-4432-8fee-b6ff369a8c86",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}