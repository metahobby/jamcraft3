{
    "id": "717ee3f6-9a8e-4cc5-8959-a4e04313f1a5",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprCoalRock",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 1,
    "bbox_right": 28,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dcb1f0d9-d28e-40c2-a4f2-d7aebeadbef5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "717ee3f6-9a8e-4cc5-8959-a4e04313f1a5",
            "compositeImage": {
                "id": "9c1d8a4e-2e05-436d-81d0-95ab527cca2a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dcb1f0d9-d28e-40c2-a4f2-d7aebeadbef5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b401b7f-dcf6-4af8-8f23-f91d7bbf7c45",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dcb1f0d9-d28e-40c2-a4f2-d7aebeadbef5",
                    "LayerId": "f11846b8-9706-4386-9e1b-5ea4c5eb799c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f11846b8-9706-4386-9e1b-5ea4c5eb799c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "717ee3f6-9a8e-4cc5-8959-a4e04313f1a5",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}