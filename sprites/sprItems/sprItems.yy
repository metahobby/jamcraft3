{
    "id": "4d421f83-b785-4857-9563-6074e386cd90",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprItems",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95b5762b-8778-4dc1-979b-58cafa595268",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "5e78b72e-b3c9-488f-b371-f419ed3da9e1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95b5762b-8778-4dc1-979b-58cafa595268",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ed918fb5-ec8a-4057-af40-8b8dad3259c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95b5762b-8778-4dc1-979b-58cafa595268",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "a7a2845f-3ea4-428b-a6e5-d386200d7c15",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "25b38d94-ad25-46fa-a08f-915d1814c31b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a7a2845f-3ea4-428b-a6e5-d386200d7c15",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2cd88f83-e9dc-4223-9c2b-accf962791dc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a7a2845f-3ea4-428b-a6e5-d386200d7c15",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "edd708d6-d5cd-40ef-8f2e-084e0532189f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "070c7bd6-9783-403b-b01b-388c38a8b425",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "edd708d6-d5cd-40ef-8f2e-084e0532189f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ef831da3-3553-4d03-a54c-5c8fd3c32ee3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "edd708d6-d5cd-40ef-8f2e-084e0532189f",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "e0af6387-dd2d-4745-a0c7-9cdff634c435",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "8a1fd1de-c516-4075-acdd-6054101b49ad",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0af6387-dd2d-4745-a0c7-9cdff634c435",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f54a4ff-3857-43c5-a3b1-1f482a158097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0af6387-dd2d-4745-a0c7-9cdff634c435",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "49410195-8cb4-43ea-a5c0-a49b49e6a238",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "6b6703fd-0aaa-4e79-90a3-7e12c752fd70",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "49410195-8cb4-43ea-a5c0-a49b49e6a238",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ddb68728-46a6-4aa1-b746-1563bc965cfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "49410195-8cb4-43ea-a5c0-a49b49e6a238",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "8a6b98e5-0ac2-4346-97c1-d3e5968c290a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "8b872c5e-4a5f-45eb-8797-4356015e115f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a6b98e5-0ac2-4346-97c1-d3e5968c290a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7fb2ab7b-c62d-4efe-a74b-e29d4b643107",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a6b98e5-0ac2-4346-97c1-d3e5968c290a",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "4e2fc603-583a-4c82-ac11-72fee92fb5d9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "36ebbf75-6802-4bd2-802a-fa7b6d8139df",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e2fc603-583a-4c82-ac11-72fee92fb5d9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d20c47cf-88b9-45f6-8749-0f815a399100",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e2fc603-583a-4c82-ac11-72fee92fb5d9",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "0dbddbeb-41c1-4c2f-a0a9-2081078f62e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "0b1c5306-0a9c-4264-abd1-5b1fa5b9c071",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0dbddbeb-41c1-4c2f-a0a9-2081078f62e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f551ac8c-6ea8-48d8-a5b2-03040235ffc7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0dbddbeb-41c1-4c2f-a0a9-2081078f62e7",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "23b47e7f-5d69-4413-b758-9083edcd7509",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "b7b522fe-78b3-4173-ba44-8562540da2ce",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "23b47e7f-5d69-4413-b758-9083edcd7509",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31af2adc-8a83-4db3-ad09-8d31dcdf62da",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "23b47e7f-5d69-4413-b758-9083edcd7509",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "fa4ee296-d8dd-474e-943b-11c96c85e575",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "25857d54-6ba2-4fe8-815a-83c114ec9dbe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fa4ee296-d8dd-474e-943b-11c96c85e575",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82457c59-bab4-4425-9ab8-06605709885f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fa4ee296-d8dd-474e-943b-11c96c85e575",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "72dcbbb7-7a77-464a-8a34-f7b002514174",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "c8f32445-e5b5-4255-9564-281c4f42aceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "72dcbbb7-7a77-464a-8a34-f7b002514174",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6144e10d-94ce-4469-b3b2-b37d59789dbb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "72dcbbb7-7a77-464a-8a34-f7b002514174",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "c068a52c-8bfc-46d5-abaf-ab518a95806d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "7a5a03f4-a7dc-4289-9bc2-1147f284f680",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c068a52c-8bfc-46d5-abaf-ab518a95806d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "364e68b9-15ef-4e2b-9345-05253c8f5be6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c068a52c-8bfc-46d5-abaf-ab518a95806d",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "1c2c32f8-672b-4291-a8bf-23422e45f9b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "5b6145f7-37f8-4b16-9b11-ac665cd46696",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1c2c32f8-672b-4291-a8bf-23422e45f9b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4e4dda88-318b-47c7-89f4-f0836b9abd97",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1c2c32f8-672b-4291-a8bf-23422e45f9b2",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "d7f27fab-1c97-4a06-9c5e-c4c217e8a123",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "5d49d09b-29d7-47fc-b8cb-56f01d605e68",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d7f27fab-1c97-4a06-9c5e-c4c217e8a123",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5f6e5659-2049-4b3a-8f22-197155e3ec87",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d7f27fab-1c97-4a06-9c5e-c4c217e8a123",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "55a300d3-065f-4ab8-85a3-e877b1657e2d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "ef83676b-a40d-421e-8e71-b38a22f63f4b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "55a300d3-065f-4ab8-85a3-e877b1657e2d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79148734-86d4-4d50-931e-0092688ab938",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "55a300d3-065f-4ab8-85a3-e877b1657e2d",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "c7a54cf3-838f-45c8-b716-773a08472cf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "ba008ddb-d793-4648-9d6c-2ca6e45709c5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7a54cf3-838f-45c8-b716-773a08472cf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e853b1ba-1cfd-4d7d-89cb-36bfd8ada9c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7a54cf3-838f-45c8-b716-773a08472cf0",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "ffed97da-13ef-45dc-b3ed-046a5fe15c3f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "8c00e3f1-377b-4eca-9a37-f7a58af48d46",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ffed97da-13ef-45dc-b3ed-046a5fe15c3f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0e4e2392-72fa-4566-bef7-62e472c99580",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ffed97da-13ef-45dc-b3ed-046a5fe15c3f",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "1d97049e-ce8f-4589-acce-776427993f65",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "d88848c9-24b1-417c-8216-9ed010604f4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1d97049e-ce8f-4589-acce-776427993f65",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "21dd78e6-0a33-4d1e-92ce-d0f88a61dbae",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1d97049e-ce8f-4589-acce-776427993f65",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "ab19b7cf-cb6e-45ac-9c0c-b9117e7db8ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "67310a8b-2846-4c4b-907e-958cc7d1c63c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ab19b7cf-cb6e-45ac-9c0c-b9117e7db8ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0dc9fb4a-72c2-4bda-a53b-bf37974073fa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ab19b7cf-cb6e-45ac-9c0c-b9117e7db8ed",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "7cb34141-e933-4e7f-ac2c-02e4396e6234",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "8f2c6565-6b73-487b-8396-5ccfb0518860",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7cb34141-e933-4e7f-ac2c-02e4396e6234",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f80cbf12-36b2-41c0-bfbe-bc6734d62700",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7cb34141-e933-4e7f-ac2c-02e4396e6234",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "e825ac56-f291-4d6a-b450-c8226e8bafcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "6c1d2f24-2ca0-49b6-8ce8-faa170bf1282",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e825ac56-f291-4d6a-b450-c8226e8bafcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e70bd1d2-24d9-4b42-aa36-c75a6d39c657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e825ac56-f291-4d6a-b450-c8226e8bafcc",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "0970afc2-8308-44fd-b16f-8bfd4d4c338c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "a20ff774-2c91-41b9-81d3-637e07f6e40f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0970afc2-8308-44fd-b16f-8bfd4d4c338c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1712432a-4f32-4d6e-ade9-2b3bcf914aef",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0970afc2-8308-44fd-b16f-8bfd4d4c338c",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "ca01c2d9-9817-48ed-9297-11987083426f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "257ff050-1929-4c53-a1ef-494054daaf22",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca01c2d9-9817-48ed-9297-11987083426f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0d5e1ba9-5070-4a90-ae29-ffebdd47df35",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca01c2d9-9817-48ed-9297-11987083426f",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "c7395179-0d9a-4769-88ee-848d0d1810dd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "4f657d28-f71e-49fe-8576-197339f651d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7395179-0d9a-4769-88ee-848d0d1810dd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d840b867-dc0c-47b5-912a-e4410427596f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7395179-0d9a-4769-88ee-848d0d1810dd",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        },
        {
            "id": "25a2bf82-8404-4dc5-98fe-d96972d56614",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "compositeImage": {
                "id": "c1f31979-be99-4ce7-ab63-973beaf16994",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "25a2bf82-8404-4dc5-98fe-d96972d56614",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1c041cf3-1e61-4499-9372-423add762ff9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "25a2bf82-8404-4dc5-98fe-d96972d56614",
                    "LayerId": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "159f95a1-b843-44d6-8bf8-b4bd8a30c80c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d421f83-b785-4857-9563-6074e386cd90",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}