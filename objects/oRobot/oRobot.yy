{
    "id": "b6fcb810-c812-4a78-9097-63baa73742a5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRobot",
    "eventList": [
        {
            "id": "33453420-fd77-4f19-a306-e7f0cee63e0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b6fcb810-c812-4a78-9097-63baa73742a5"
        },
        {
            "id": "dce62c55-992e-415b-8fbc-68aacca13e21",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b6fcb810-c812-4a78-9097-63baa73742a5"
        },
        {
            "id": "304b04a6-8413-4bad-a149-45fd5765292a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b6fcb810-c812-4a78-9097-63baa73742a5"
        },
        {
            "id": "920de4f6-dab6-4704-b4e9-3becbe0f8c02",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "c33adb9e-83d6-4032-88e7-8b4a1e2fb28f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b6fcb810-c812-4a78-9097-63baa73742a5"
        },
        {
            "id": "9ac38b76-a655-46b6-8fc0-35482c22be14",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "286c6e0e-94ff-4eb9-8c5b-9a53701a27e4",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "b6fcb810-c812-4a78-9097-63baa73742a5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "76ccd929-9a98-40f7-80f3-8b80f19f711b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "37d02ee1-99cc-4ce5-8aec-f89940cc5cd3",
    "visible": true
}