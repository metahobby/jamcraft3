/// @desc Config

image_xscale = .5;
image_yscale = .5;

flameThrowSFX = false; //Is the sfx playing / looping now
limitedLife = false; //same sfx loop flag thing

//drill

drillFrame = 0;
drillPoint = 0;
drillOffsetX = sprite_width - 4;
drillOffsetY = 0;

//VISION IS OFF NOW
//vision = instance_create_depth(x,y,depth-1,oRobotVision);
vision = -42 //TODO better vision cone?
visionOffsetX = 0;
visionOffsetY = -21;
lightRadius = 2000; //emits from robot center

step = 0;

playerNum = 0;

keyLeft = false;
keyRight = false;
keyUp = false;
keyDown = false;
keyJump = false;


dying = false; //TODO health dying mechanics

//TODO tweak this for upgrades / robots etc, it's a lot of things!
frictionRate = 1;
frictionFactor = 0.1;

hsp = 0;
thresh = 1;
walkRate = 1;
walksp = 0.3;
maxwalksp = 2;

grv = 0.4;

vsp = 0;
maxvsp = 9;

jumppow = 8;
maxJumpCharges = 1;
jumpCharges = 0;

