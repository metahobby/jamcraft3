/// @desc ARM DRILL AND DRAW
draw_self();

if mouse_check_button(mb_left) || gamepad_button_check(0,gp_face2) {
	if image_xscale >= 0 {
		draw_sprite_ext(sprDrill, drillFrame,x + drillOffsetX,y + drillOffsetY, .5,.5,image_angle,image_blend, 1)
		drillPoint = x + drillOffsetX;
	} else if image_xscale <0 {
		draw_sprite_ext(sprDrill, drillFrame,x - drillOffsetX,y + drillOffsetY, -.5,.5,image_angle,image_blend, 1)
		drillPoint = x - drillOffsetX;
	}
	if step % 60 {
		drillFrame++;
		if drillFrame > sprite_get_number(sprDrill) - 1 {
			drillFrame = 9;
		}
	}
	if step % 60 == 0 {
		show_debug_message(drillPoint)
	}
	
	var drillHitting = instance_place(drillPoint, y+1, oSilicon)
	if drillFrame >= 9 && instance_exists(drillHitting) {
		show_debug_message("TODO LOOT!")
		invGainItem(oGame.inv, items.coalRock, 1, false)
		effect_create_below(ef_spark, drillPoint, y, 0.1, c_white);
		audio_play_sound(sfxCollectItem, 1, false);
		with drillHitting {
			oGame.rocksMined++;
			if sprite_index == sprQuartzRock {
				oGame.quartzMined++
			}
			if sprite_index == sprCoalRock {
				oGame.coalMined++
			}
			instance_destroy()
		}
	}
} else {
	if drillFrame > 0 {
		drillFrame--
		draw_sprite_ext(sprDrill, drillFrame,x + 
		(image_xscale == .5 ? drillOffsetX :  (-1 * drillOffsetX)),
		y + drillOffsetY, image_xscale, image_yscale,image_angle,image_blend, 1)
	}
}

if mouse_check_button_released(mb_left) || gamepad_button_check_released(0,gp_face2) {	
	drillPoint = 0;
}

var pdir = point_direction(x,y,mouse_x,mouse_y)
var armImageIndex = 0; 

if pdir >= 0 && pdir < 45 {
	if image_xscale >= 0 {
		armImageIndex = 4;
	} else {
		armImageIndex = 2;
	}
}

if pdir >= 45 && pdir < 90 {
	if image_xscale >= 0 {
		armImageIndex = 3;
	} else {
		armImageIndex = 3;
	}
}

if pdir >= 90 && pdir < 135 {
	if image_xscale >= 0 {
		armImageIndex = 3;
	} else {
		armImageIndex = 3;
	}
}

if pdir >= 135 && pdir < 180 {
	if image_xscale >= 0 {
		armImageIndex = 2;
	} else {
		armImageIndex = 4;
	}
}

if pdir >= 180 && pdir < 225{
	if image_xscale >= 0 {
		armImageIndex = 1;
	} else {
		armImageIndex = 4;
	}
}

if pdir >= 225 && pdir < 270{
	if image_xscale >= 0 {
		armImageIndex = 0;
	} else {
		armImageIndex = 0;
	}
}

if pdir >= 270 && pdir < 315{
	if image_xscale >= 0 {
		armImageIndex = 0;
	} else {
		armImageIndex = 0;
	}
}

if pdir >= 315 && pdir < 360{
	if image_xscale >= 0 {
		armImageIndex = 4;
	} else {
		armImageIndex = 1;
	}
}



draw_sprite_ext(sprArm, armImageIndex,x,y - 18, image_xscale, image_yscale,image_angle,image_blend, 1)

/**/ //Light Radiusa 
if !surface_exists(oGame.surfaceWithMask) {
	oGame.surfaceWithMask = surface_create(room_width, room_height);
}

surface_set_target(oGame.surfaceWithMask)
gpu_set_blendmode(bm_subtract)
draw_set_color(c_black)

if keyJumpHold {
	draw_set_alpha(0.03);
} else {
	draw_set_alpha(0.02);
}
for (var i = 40; i > 0; i--) {
	draw_circle(x,y-sprite_height/2,
	(lightRadius * (keyJumpHold ? 1.2 : 1) )/i
	,false)
}
draw_set_alpha(1)

draw_sprite_ext(sprRobotEyesMask, armImageIndex,x,y, image_xscale, image_yscale,image_angle,image_blend, 1)
draw_sprite_ext(sprRobotPanelMask, armImageIndex,x,y, image_xscale, image_yscale,image_angle,image_blend, 1)
if keyJumpHold && vsp != 0 &&!place_meeting(x, y + 10, oSolid)  {
 draw_sprite_ext(sprRobotJumpMask, armImageIndex,x,y, image_xscale, image_yscale,image_angle,image_blend, 1)
}
gpu_set_blendmode(bm_normal)

surface_reset_target();