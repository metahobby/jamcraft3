/// @desc Movement / Control


if !dying {
	if oGame.hitPoints < 3 && !limitedLife {
		limitedLife = true;
		audio_play_sound(sfxLimitedLife, 1, true)
	} else if oGame.hitPoints > 2 {
		limitedLife = false;
		audio_stop_sound(sfxLimitedLife)
	}
	
	if oGame.hitPoints <= 0 {
		dying = 20;
	}
}

if playerNum == 1 {
	keyLeft = gamepad_axis_value(0,0) < -.5 || keyboard_check(ord("A"));
	keyRight = gamepad_axis_value(0,0) >= .5 || keyboard_check(ord("D"));
	keyUp = gamepad_axis_value(0,1) > .5 || keyboard_check(ord("W"));
	keyDown = gamepad_axis_value(0,1) <= -.5 || keyboard_check(ord("S"));
	keyJump = gamepad_button_check_pressed(0,gp_face1) || keyboard_check_pressed(vk_space);
	keyJumpHold = gamepad_button_check(0,gp_face1) || keyboard_check(vk_space);
} else if playerNum == 2 {
	keyLeft = gamepad_axis_value(1,0) < -.5 || keyboard_check(vk_left);
	keyRight = gamepad_axis_value(1,0) >= .5 || keyboard_check(vk_right);
	keyUp = gamepad_axis_value(1,1) > .5 || keyboard_check(vk_up);
	keyDown = gamepad_axis_value(1,1) <= -.5 || keyboard_check(vk_down);
	keyJump = gamepad_button_check_pressed(1,gp_face1) || keyboard_check_pressed(vk_control);
} else if playerNum == 3 {
	keyLeft = gamepad_axis_value(2,0) < -.5
	keyRight = gamepad_axis_value(2,0) >= .5
	keyUp = gamepad_axis_value(2,1) > .5
	keyDown = gamepad_axis_value(2,1) <= -.5
	keyJump = gamepad_button_check_pressed(2,gp_face1)
} else if playerNum == 4 {
	keyLeft = gamepad_axis_value(3,0) < -.5
	keyRight = gamepad_axis_value(3,0) >= .5
	keyUp = gamepad_axis_value(3,1) > .5
	keyDown = gamepad_axis_value(3,1) <= -.5
	keyJump = gamepad_button_check_pressed(3,gp_face1)
}

if dying {
		keyLeft = false;
		keyRight = false;
		keyUp = false;
		keyDown = false;
		keyJump = false;
		dying--
}

if dying == 1 { //LAST DEATH FRAME
	room_goto(rmGame)
	x = xstart;
	y = ystart;
	hitPoints = 10;
}

//SFX //TODO


// TODO ZONE

// TODO Re-add ducking effecting the camera in single player?
// TODO Multiplayer or not?
// TODO Death needs added again

// SIDE TO SIDE MOVEMENT

// Friction happens first!
// At a rate we define for each robot
if oGame.step % frictionRate == 0 {
	if hsp != 0 {
		if hsp < 0.2 && hsp > -0.2 { // stopping thresh
			hsp = 0;
		} else if hsp > 0 {
			hsp -= frictionFactor;
		} else if hsp < 0 {
			hsp += frictionFactor;
		}
	}
}

// The player gains speed only on walk steps
if oGame.step % walkRate == 0 {
var move = keyRight - keyLeft; //no right and left wigglin
if (hsp + move * walksp) < maxwalksp // We have limits to our speed
	and (hsp + move * walksp) > -maxwalksp // Both directions
hsp += move * walksp;
} //TODO approach all the way to maxwalksp...

 // we'll hit a solid going left or right
if place_meeting(x + hsp, y, oSolid) {
	while !place_meeting(x + sign(hsp)/10, y, oSolid) {
		x += sign(hsp)/10;
	}
	hsp = 0;
}

x += hsp;

// RISING AND FALLING MOVEMENT

// Gravity and Friction happen first
if oGame.step % frictionRate == 0 {
	if vsp != 0 {
		if vsp > 0{
			vsp -= frictionFactor;
		} else if hsp < 0 {
			vsp += frictionFactor;
		}
	}
}

if vsp < maxvsp {
	vsp += grv;
}
		
// jumping on the floor
if place_meeting(x, y + 0.1, oSolid){
	jumpCharges = maxJumpCharges;
	if flameThrowSFX {
		audio_stop_sound(sfxFlameThrowLoop)
		flameThrowSFX = false;
	}
	if keyJump {
		vsp = - jumppow;
		audio_play_sound(sfxRobotJumps, 1, false);
	}
}

// Jumpming in the air
if keyJump and jumpCharges {
	vsp = - jumppow;
	jumpCharges--;
	audio_play_sound(sfxRobotJumps, 1, false);
}

// Upper and lower collision
if place_meeting(x, y + vsp, oSolid) { // We'll hit a solid
	// Regular Solid
	// So go toward it till we do hit it.
	while !place_meeting(x, y + sign(vsp)/10, oSolid) {
		y += sign(vsp)/10;
	}
	//Then stop.
	if vsp > 3 {
			audio_play_sound(sfxRobotLands, 1, false);
	}
	vsp = 0;
}

// Hover on hold!
if vsp > 0 && keyJumpHold {
	vsp /= 1.2;
	if !flameThrowSFX {
	flameThrowSFX = true;
	audio_play_sound(sfxFlameThrowLoop, 1, true);
	}
} else if flameThrowSFX && !keyJumpHold {
	audio_stop_sound(sfxFlameThrowLoop)
	flameThrowSFX = false;
}

y += vsp;


// Animations
// TODO MAKE THESE WORK WITH SPRITES AS WE GET THEM FROM ARTSITS
// TODO REQUEST NEEDED ART

//TODO color tint? //image_blend = robotColor;

if hsp > .2 { // Moving Right.
	image_xscale = .5;
} else if hsp < -.2 { // Moving left.
	image_xscale = -.5;
} else {
	if keyLeft {
		image_xscale = -.5;
	} else if keyRight {
		image_xscale = .5;
	}
	// don't change it
}

// Jumping
if !place_meeting(x, y + 0.1, oSolid) {
	if keyJump {
		sprite_index = sprRobotJump//jumping;
	} 
	image_speed = 1;
} else {
	image_speed = 1;
	if (round(hsp) == 0) { //Idle
		if keyDown {
			sprite_index = sprRobot//sprPlayerDownIdle;
		} else if keyUp {
			sprite_index = sprRobot//sprPlayerUpIdle;
		} else {
			sprite_index = sprRobot//sprPlayerIdle;
		}
	} else { //Running
		sprite_index = sprRobot//sprPlayerRun;
	}
}

//If dead, show dead!
if dying > 1 {
	sprite_index = sprRobotDead
} else if dying == 1 {
	sprite_index = sprRobot; //REBOOT!
}



//have vision follow
if instance_exists(vision) {
	vision.x = x + visionOffsetX;
	vision.y = y + visionOffsetY;
}
step++