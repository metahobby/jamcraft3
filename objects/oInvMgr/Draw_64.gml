/// @desc draw if moving items

var mID = oGame.mouseSlot[# 0, 0];


//Draw stuff
if (mID != items.none) {
	var _x = device_mouse_x_to_gui(0) - sprite_get_width(sprInvSlot) / 2;
	var _y = device_mouse_y_to_gui(0) - sprite_get_height(sprInvSlot);
	draw_sprite(sprItems, mID, _x, _y); //Draw item sprite
}