/// @description Insert description here
// You can write your code in this editor
itemPlaced = true;
nSlots = ds_grid_width(oGame.inv);
nRows = 3;
prevSlot = -1;
prevInv = -1;
inDrag = false;
var _slot = 0;

var yOffSize = sprite_get_height(sprInvSlot) + sprite_get_height(sprInvSlot) / 10;
var yOff = oGameMenu.firstYOffset * 2;
var slotPerRow = nSlots / nRows;
var xOffSize = (oGameMenu.menuWidth - 20) / slotPerRow; 
if !layer_exists("lrInv") {
	layer_create(objDepth.gui, "lrInv");
}
while (_slot < nSlots)
{
	var xOff = (oGameMenu.x - ((xOffSize * slotPerRow) / 2)) + (sprite_get_width(sprInvSlot) / 4);
	for (var s = 0; s < slotPerRow; s++) {
		var inst = instance_create_layer(oGameMenu.x , oGameMenu.y ,  "lrInv", oInvSlot);
		inst.inv = oGame.inv;
		inst.slot = _slot;
		inst.followParent = oGameMenu;
		inst.yOff = yOff;
		inst.xOff = xOff;
		xOff += xOffSize;
		_slot++;
	}
	yOff += yOffSize;
}
