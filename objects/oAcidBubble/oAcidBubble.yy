{
    "id": "74c08515-7012-41c1-957c-5fe4e1c7f0eb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAcidBubble",
    "eventList": [
        {
            "id": "39cd75f6-92d7-4b39-a0db-2d5239a1c510",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "74c08515-7012-41c1-957c-5fe4e1c7f0eb"
        },
        {
            "id": "cce520e9-a25e-48b0-a2b9-31c7c32d774f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "74c08515-7012-41c1-957c-5fe4e1c7f0eb"
        },
        {
            "id": "c0f193f9-37d4-4c7e-9a2d-13458b693c37",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "74c08515-7012-41c1-957c-5fe4e1c7f0eb"
        },
        {
            "id": "bc5ab8e5-6fb2-44e9-a214-cb0583d9f9d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b6fcb810-c812-4a78-9097-63baa73742a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "74c08515-7012-41c1-957c-5fe4e1c7f0eb"
        },
        {
            "id": "e77606a0-8ac4-4498-bb1d-340b7166be0c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "76ccd929-9a98-40f7-80f3-8b80f19f711b",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "74c08515-7012-41c1-957c-5fe4e1c7f0eb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e6a5592d-6e3c-4f67-8e11-64d2944f5776",
    "visible": true
}