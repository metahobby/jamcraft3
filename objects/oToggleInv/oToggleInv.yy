{
    "id": "9dc38e94-6dad-4ebf-9e12-d64baf905827",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oToggleInv",
    "eventList": [
        {
            "id": "aa7e7884-d9f3-4bfd-a7f3-53b69c128426",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "9dc38e94-6dad-4ebf-9e12-d64baf905827"
        },
        {
            "id": "774cd648-e05a-4c1a-a663-c74fa5058a2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "9dc38e94-6dad-4ebf-9e12-d64baf905827"
        },
        {
            "id": "cfa21600-938d-43a3-8714-20020f1e2116",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9dc38e94-6dad-4ebf-9e12-d64baf905827"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c00142a4-7144-449a-b3cc-a9fc1b88b83c",
    "visible": true
}