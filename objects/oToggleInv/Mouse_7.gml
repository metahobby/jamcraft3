/// @desc toggle open close
if oGameMenu.menuOpen == false {
	audio_play_sound(sfxSlotGrab, 1, false);
	oGameMenu.stepsForceOpen = 30;
} else {
	audio_play_sound(sfxSlotDrop, 1, false);
	oGameMenu.stepsForceClose = 30;
}
