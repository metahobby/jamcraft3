{
    "id": "084a2172-398c-4945-802d-0205eae71f8a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSilicon",
    "eventList": [
        {
            "id": "71df7604-b3f6-4ed7-80dc-57c4ae50a0b0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "084a2172-398c-4945-802d-0205eae71f8a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "76ccd929-9a98-40f7-80f3-8b80f19f711b",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b6c7ad08-e227-42f4-86d0-6061f4747a23",
    "visible": true
}