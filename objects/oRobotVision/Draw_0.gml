/// @desc ?
if !surface_exists(oGame.surfaceWithMask) {
	oGame.surfaceWithMask = surface_create(room_width, room_height);
}

surface_set_target(oGame.surfaceWithMask)
gpu_set_blendmode(bm_subtract)
draw_self()
gpu_set_blendmode(bm_normal)
surface_reset_target();