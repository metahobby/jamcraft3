{
    "id": "ffb708e3-864b-4875-8aef-3903074438c5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oRobotVision",
    "eventList": [
        {
            "id": "6beebd89-37d7-41c4-8334-40c4df90afbc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ffb708e3-864b-4875-8aef-3903074438c5"
        },
        {
            "id": "b7d380c8-1205-447d-8564-2ba4b061da44",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "ffb708e3-864b-4875-8aef-3903074438c5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "67bd8606-e142-4c93-8a94-7261034d15be",
    "visible": true
}