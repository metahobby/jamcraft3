/// @desc draw as mask
if !surface_exists(oGame.surfaceWithMask) {
	oGame.surfaceWithMask = surface_create(room_width, room_height);
}
surface_set_target(oGame.surfaceWithMask)
gpu_set_blendmode(bm_subtract)

draw_set_color(c_black)

//draw_self()
for (var  i = room_height - 100; i > 0; i-= 10){
	distFromSunCone = i / room_height
	if distFromSunCone > .8 {
		draw_set_alpha(.2)
	}
	if distFromSunCone > .5 {
		draw_set_alpha(.5)
	}
	if distFromSunCone > .2 {
		draw_set_alpha(.1)
	}
	for (var  j = 0; j < distFromSunCone * 100; j++){
		if j % 6 == 0 {
			draw_circle(x,y+i, j, false)
		}
	}
}

draw_set_alpha(1)

gpu_set_blendmode(bm_normal)
surface_reset_target();