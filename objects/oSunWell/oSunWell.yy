{
    "id": "2c792bbb-660f-41cc-939e-d4d2ea343a9a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSunWell",
    "eventList": [
        {
            "id": "cfb315d7-ed88-422c-8566-3864838f44db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2c792bbb-660f-41cc-939e-d4d2ea343a9a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "63d8aefc-b069-40d6-a55e-8da37a642ea9",
    "visible": true
}