{
    "id": "4419d7be-48f9-4a40-9f77-23afb03f6ce4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGame",
    "eventList": [
        {
            "id": "98cf3f3a-3284-4bc7-9863-f7e35a73b442",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4419d7be-48f9-4a40-9f77-23afb03f6ce4"
        },
        {
            "id": "479bd967-bf41-4343-ac36-b2dd9cb1889c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4419d7be-48f9-4a40-9f77-23afb03f6ce4"
        },
        {
            "id": "6f421e54-97c0-4d23-82c1-6a4be8c4ab96",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "4419d7be-48f9-4a40-9f77-23afb03f6ce4"
        },
        {
            "id": "0f9206ec-525b-4b1e-8f42-3173edd7b11b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "4419d7be-48f9-4a40-9f77-23afb03f6ce4"
        },
        {
            "id": "814f3bee-76fa-4907-889d-3308cd0b964c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "4419d7be-48f9-4a40-9f77-23afb03f6ce4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}