/// @desc ?

if keyboard_check_pressed(vk_f12) {
	debug = !debug;
}

if keyboard_check_pressed(vk_f5) {
	game_restart();
}

if keyboard_check_pressed(vk_f8) {
	gifSave = true;
	show_debug_message("go go gifsaver!")
}

if gifSave == true {
	if gifFrameCount == 0 {
	    gifImage = gif_open(room_width, room_height);
	} else if gifFrameCount < 60 {
		gif_add_surface(gifImage, application_surface, 6/100);
	} else {
	    gif_save(gifImage, "Capture" + string(gifCount) + ".gif");
		show_debug_message("gifsaver success: " +
			"Capture" + string(gifCount) + ".gif");
		gifCount++;
	    gifFrameCount = 0;
	    gifSave = false;
	}
	gifFrameCount++;
}

step++;