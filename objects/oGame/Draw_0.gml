/// @desc ?

if !surface_exists(surfaceWithMask) {
	surfaceWithMask = surface_create(room_width, room_height);
}

surface_set_target(surfaceWithMask)

//The darkness
draw_set_color(c_black)
draw_set_alpha(1) //pure darkness
draw_rectangle(0,0,room_width,room_height,false)

gpu_set_blendmode(bm_subtract)

// farther, darker
var distToMouse = point_distance(oRobot.x, oRobot.y, mouse_x, mouse_y)
if distToMouse > 300{
	draw_set_alpha(.01)
} else if distToMouse > 250{
	draw_set_alpha(.03)
} else if distToMouse > 200{
	draw_set_alpha(.06)
} else if distToMouse > 150{
	draw_set_alpha(.07)
} else {
	draw_set_alpha(.1)
}

for (var i = 40; i > 0; i--) {
	draw_circle(mouse_x,mouse_y,
	i,false)
}

draw_set_alpha(1)
gpu_set_blendmode(bm_normal)
surface_reset_target();
