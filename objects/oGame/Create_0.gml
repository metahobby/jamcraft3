/// @desc ?
surfaceWithMask = -42;//Set in draw!
quartzMined = 0;
coalMined = 0;
rocksMined = 0;
hitPoints = 5;

audio_play_sound(bgmCaveRobotGamePlay,1,true);

//draw_set_font(TODO FONT);
draw_set_valign(fa_top);
draw_set_halign(fa_left);

global.touchActive = false;
gameWidth = 640;
gameHeight = 360;
global.cDefTop = c_white;
global.cDefMid = c_silver;
global.cDefBot = c_black;
ccArr = array_create(3, 0);
ccArr[0] = c_yellow;
ccArr[1] = c_silver;
ccArr[2] = c_orange;

qcArr = array_create(5, 0);
qcArr[0] = c_ltgray;
qcArr[1] = c_green;
qcArr[2] = c_blue;
qcArr[3] = c_purple;
qcArr[4] = c_yellow;
//fonts
global.fontGUI = createGuiFont("Retron2000.ttf", 22);
global.font = createGuiFont("Retron2000.ttf", 40);

step = 0;
debug = false;

gifImage = -42; //Used for saving the gif
gifSave = false; // Are we currently saving a gif?
gifCount = 0; // How many gifs have we saved
gifFrameCount = 0; //What frame are we on?

p1 = -42 // Set on instance.
p2 = -42 // Set on instance.
p3 = -42 // Set on instance.
p4 = -42 // Set on instance.

loadItemDex();
inv = -42 // Set in loadInventory();
loadInventory();
invMax = 18;

mouseSlot = ds_grid_create(1, 3);
if !layer_exists("lrGameGUI") {
	layer_create(objDepth.gui + 100, "lrGameGUI");
}

instance_create_layer(0,0, "lrGameGUI", oGameMenu);
instance_create_layer(0,0, "lrGameGUI", oInvMgr);