{
    "id": "38b5422e-9b87-4946-8c7c-23db3b4930c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oAcidPit",
    "eventList": [
        {
            "id": "190391a0-05ef-4954-be5d-be10aa84ece4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "38b5422e-9b87-4946-8c7c-23db3b4930c2"
        },
        {
            "id": "4025ea33-341d-42bf-a8bc-7ec8420267ce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "38b5422e-9b87-4946-8c7c-23db3b4930c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b433661f-c463-4765-8e2c-f4cbd66ddc72",
    "visible": true
}