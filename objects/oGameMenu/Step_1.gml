/// @desc animate up or down

if y > midPos {
	menuOpen = false;
} else {
	menuOpen = true;
}

if stepsForceClose > 0 {
	stepsForceClose--
	menuOpen = false;
}

if stepsForceOpen > 0 {
	stepsForceOpen--
	menuOpen = true;
}