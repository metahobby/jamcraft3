/// @desc

image_speed = 0;
inDrag = false;
menuOpen = false;
moveSpeed = 20;

stepsForceClose = 60;
stepsForceOpen = 0;

var _height = oGame.gameHeight;
var _width = oGame.gameWidth;

firstYOffset = _height / 18;
menuWidth = _width - ((_width / 30) * 2);

x = oCamera.x;
closePos = oCamera.y + oGame.gameHeight * .45;
openPos = oCamera.y;
midPos = openPos + ((closePos - openPos) / 2);

if !layer_exists("lrInv") {
	layer_create(objDepth.gui, "lrInv");
}


instance_create_layer(x, y, "lrInv", oToggleInv);