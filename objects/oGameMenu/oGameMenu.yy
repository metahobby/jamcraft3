{
    "id": "10b9377b-deaa-4780-8c9f-4e1a289c5861",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oGameMenu",
    "eventList": [
        {
            "id": "a49d1102-5d68-45fe-8e08-aab3ae65c18a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "10b9377b-deaa-4780-8c9f-4e1a289c5861"
        },
        {
            "id": "c184626b-b312-4114-aee7-7343ff53182a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10b9377b-deaa-4780-8c9f-4e1a289c5861"
        },
        {
            "id": "7759a175-35dd-4aae-b259-cc2c29f9da82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "10b9377b-deaa-4780-8c9f-4e1a289c5861"
        },
        {
            "id": "37bbe59d-65eb-4aca-8bfe-9d9b3d774e74",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "10b9377b-deaa-4780-8c9f-4e1a289c5861"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f01a5479-de3c-4409-9d47-9dab612f56b1",
    "visible": true
}