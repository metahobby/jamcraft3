/// @desc follow camera

x = oCamera.x;
closePos = oCamera.y + oGame.gameHeight * .45;
openPos = oCamera.y;
midPos = openPos + ((closePos - openPos) / 2);

if y != openPos || y != closePos {
	var targPos = menuOpen == true ? openPos : closePos;
	y = approach(y, targPos, moveSpeed);
	y = clamp(y, openPos, closePos);
}