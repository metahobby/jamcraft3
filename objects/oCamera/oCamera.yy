{
    "id": "0b0915f7-3d8d-4ce8-be22-59db40bd9739",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oCamera",
    "eventList": [
        {
            "id": "13eea6e4-a839-4038-b161-f6d58db09bf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0b0915f7-3d8d-4ce8-be22-59db40bd9739"
        },
        {
            "id": "42c2bc02-ba3b-4132-9d66-7a50dc45df31",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0b0915f7-3d8d-4ce8-be22-59db40bd9739"
        },
        {
            "id": "1f94a92d-b31c-4428-8e56-c3baa503b13c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0b0915f7-3d8d-4ce8-be22-59db40bd9739"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}