/// @description Insert description here
// You can write your code in this editor
if inv == undefined 
	|| (inv == oGame.inv && slot >= oGame.invMax) {
	return;
}

if !oInvMgr.inDrag && checkGamePressed() {
	slotPressed = true;
}

if slotPressed && checkGame() {
	pressStep++;
} else {
	slotPressed = false;
	pressStep = 0;
}

var _id = inv[# slot, 0];
var amount = inv[# slot, 1];
var quality = inv[# slot , 2];
var mID = oGame.mouseSlot[# 0, 0];
var mAmt = oGame.mouseSlot[# 0, 1];
var mQual = oGame.mouseSlot[# 0, 2];

if oInvMgr.inDrag && checkGameRelease() { // we are dropping an item.
	audio_play_sound(sfxSlotDrop, 1, 0); 
	if _id == 0 { // we are dropping into an empty slot
		inv[# slot, 0] = mID;
		inv[# slot, 1] = mAmt;
		inv[# slot, 2] = mQual;
	} else { // the slot is not empty
		oInvMgr.prevInv[# oInvMgr.prevSlot, 0] = inv[# slot, 0];
		oInvMgr.prevInv[# oInvMgr.prevSlot, 1] = inv[# slot, 1];
		oInvMgr.prevInv[# oInvMgr.prevSlot, 2] = inv[# slot, 2];
		inv[# slot, 0] = mID;
		inv[# slot, 1] = mAmt;
		inv[# slot, 2] = mQual;
	}
	invEditSlotAmt(oGame.mouseSlot, 0, 0, 0, true);
	oInvMgr.prevSlot = -1;
	oInvMgr.itemPlaced = true;
	oInvMgr.inDrag = false;
	global.touchActive = false;
} else if !oInvMgr.inDrag && slotPressed && pressStep > holdDelay { // we are picking up a new item.
	audio_play_sound(sfxSlotGrab, 1, 0);
	global.touchActive = true;
	invEditSlotAmt(inv, slot, 0, 0, true);
	oGame.mouseSlot[# 0, 0] = _id;
	oGame.mouseSlot[# 0, 1] = amount;
	oGame.mouseSlot[# 0, 2] = quality;
	oInvMgr.inDrag = true;
	oInvMgr.itemPlaced = false;
	oInvMgr.prevSlot = slot;
	oInvMgr.prevInv = inv;
	pressStep = 0;
	slotPressed = 0;
}



