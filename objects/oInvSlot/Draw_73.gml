/// @desc draw override

if followParent != undefined {
	x = followParent.x + xOff;
	y = followParent.y + yOff;
}

if glowStep > 0 {
	glowStep--;
	draw_set_blend_mode(bm_add)
	for(c = 0;c < 360;c += 18){
	draw_sprite_ext(sprite_index,1,x+lengthdir_x(4,c),y+lengthdir_y(4,c),image_xscale,image_yscale,image_angle,c_red,image_alpha*0.25)
	}
	draw_set_blend_mode(bm_normal)
}

draw_self();

if inv == undefined 
	|| (inv == oGame.inv && slot >= oGame.invMax)
	{
	draw_sprite(sprInvLock, 0, x + 1, y + 1);
	return;
}

var _id = inv[# slot, 0];
var quality = inv[# slot, 2];
var color = getItemQualityColor(_id, quality);


//Draw stuff
if (_id != items.none) {
	draw_sprite(sprItemIcons, _id, x, y); //Draw item sprite
	draw_circle_color(x + 5, y + 5, 3, color, color, false); //Draw item quantity

}
