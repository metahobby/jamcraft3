{
    "id": "9232a9aa-d2e4-4e09-a532-f36f80b4ecbd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oInvSlot",
    "eventList": [
        {
            "id": "ce81c05b-faa5-490e-b5a9-adbdfb1d3c90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9232a9aa-d2e4-4e09-a532-f36f80b4ecbd"
        },
        {
            "id": "00ce6d55-c244-4805-8b4a-f4643b0b8b0b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 3,
            "m_owner": "9232a9aa-d2e4-4e09-a532-f36f80b4ecbd"
        },
        {
            "id": "932ee92a-2000-418f-87c7-6fecf1dacd2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "9232a9aa-d2e4-4e09-a532-f36f80b4ecbd"
        },
        {
            "id": "0d01ad7e-bfbf-41c5-ba24-2265f5bdcb47",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 3,
            "m_owner": "9232a9aa-d2e4-4e09-a532-f36f80b4ecbd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a6fa89b6-7321-4cb3-b665-cff5560eb86e",
    "visible": true
}