/// @desc init

depth -= 1;

// what type of slot is this slotTypes (enum)
type = undefined;

// what inventory is it apart of
inv = undefined;

// which slot does it represent
slot = 0;

// which object should the slot follow
followParent = undefined;

// the x and y off from the follow parent.
xOff = 0;
yOff = 0;

// glow variables.
glowStep = 0;
glowDuration = 30;


inDrag = false;
slotPressed = false;
pressStep = 0;
holdDelay = 2;
