/// @description Insert description here
// You can write your code in this editor

if oInvMgr.inDrag && !mouse_check_button(mb_any) && oInvMgr.itemPlaced == false {
		if invHasEmptySlot(oGame.inv) {
			audio_play_sound(sfxSlotDrop, 1 ,0);
			invGainItem(oGame.inv, oGame.mouseSlot[# 0, invStat.item], oGame.mouseSlot[# 0, invStat.amt], false);
		} else {
			oToggleInv.glowStep = oToggleInv.glowDuration;
			//audio_play_sound(sfxUIError, 1, 0);
			invGainItem(oInvMgr.prevInv, oGame.mouseSlot[# 0, invStat.item], oGame.mouseSlot[# 0, invStat.amt], false);
		}
		invEditSlotAmt(oGame.mouseSlot, 0, 0, 0, true);
		oInvMgr.inDrag = false;
		global.touchActive = false;
		oInvMgr.prevSlot = -1;
		oInvMgr.prevInv = -1;
		oInvMgr.itemPlaced = true;
}