{
    "id": "f1292dd2-25e9-47cd-aecb-e6cdedbf1e6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "oSpooder",
    "eventList": [
        {
            "id": "27b2af3e-6c2e-4d45-8902-bec6a9109f2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f1292dd2-25e9-47cd-aecb-e6cdedbf1e6f"
        },
        {
            "id": "c304c460-b9f3-4441-b3a7-47d5b77319a1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f1292dd2-25e9-47cd-aecb-e6cdedbf1e6f"
        },
        {
            "id": "15b8ac8f-ba76-42f6-b4d1-70946453959a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f1292dd2-25e9-47cd-aecb-e6cdedbf1e6f"
        },
        {
            "id": "a39b7e42-800d-4bf2-8822-23b6908fc83d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "b6fcb810-c812-4a78-9097-63baa73742a5",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f1292dd2-25e9-47cd-aecb-e6cdedbf1e6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c6925877-8b16-45bd-bfb3-f87739fbbb41",
    "visible": true
}