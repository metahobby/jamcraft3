/// @desc Config
step = 0;

playerNum = 0;

keyLeft = false;
keyRight = false;
keyUp = false;
keyDown = false;
keyJump = false;

dying = false; //TODO health dying mechanics

//TODO tweak this for upgrades / robots etc, it's a lot of things!
frictionRate = 1;
frictionFactor = 0.1;

hsp = 0;
thresh = 1;
walkRate = 1;
walksp = 0.3;
maxwalksp = 2;

grv = 0.3;

vsp = 0;
maxvsp = 3;

jumppow = 8;
maxJumpCharges = 1;
jumpCharges = 0;